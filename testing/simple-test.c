/* GDA Test program
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-client.h"
#include "gda-common-decls.h"

static void show_info (GdaConnection *cnc);

int
main (int argc, char *argv[])
{
	GdaConnection *cnc;

	gda_client_init ("simple-test", VERSION, argc, argv);

	cnc = gda_connection_new ("OAFIID:GNOME_Database_Provider_Default", 0);
	if (!cnc)
		g_error (_("Could not activate connection object"));

	show_info (cnc);

	gda_connection_free (cnc);

	return 0;
}

static void
show_info (GdaConnection *cnc)
{
	gchar *str;

	str = gda_connection_get_version (cnc);
	g_print (_("\tVersion:\t%s\n"), str);
	g_free (str);
}
