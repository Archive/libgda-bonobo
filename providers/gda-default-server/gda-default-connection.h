/* GDA Default provider
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Library; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_default_connection_h__)
#  define __gda_default_connection_h__

#include "gda-server-connection.h"
#include "build_sqlite/sqlite.h"

#define GDA_DEFAULT_CONNECTION_TYPE        (gda_default_connection_get_type ())
#define GDA_DEFAULT_CONNECTION(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_DEFAULT_CONNECTION_TYPE, GdaDefaultConnection))
#define GDA_DEFAULT_CONNECTION_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_DEFAULT_CONNECTION_TYPE, GdaDefaultConnectionClass))
#define GDA_IS_DEFAULT_CONNECTION(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_DEFAULT_CONNECTION_TYPE))
#define GDA_IS_DEFAULT_CONNECTION_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_DEFAULT_CONNECTION_TYPE))

typedef struct _GdaDefaultConnection      GdaDefaultConnection;
typedef struct _GdaDefaultConnectionClass GdaDefaultConnectionClass;

struct _GdaDefaultConnection {
	GdaServerConnection connection;

	/* custom data for this class */
	sqlite *sqlite;
};

struct _GdaDefaultConnectionClass {
	GdaServerConnectionClass parent_class;
};

GType                    gda_default_connection_get_type (void);

#endif
