/* GDA Default provider
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Library; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-common-decls.h"
#include "gda-default.h"
#include "gda-quark-list.h"

#define PARENT_TYPE GDA_SERVER_CONNECTION_TYPE

static void gda_default_connection_class_init    (GdaDefaultConnectionClass *klass);
static void gda_default_connection_instance_init (GObject *object, GTypeClass *klass);
static void gda_default_connection_finalize      (GObject *object);

static GNOME_Database_ErrorCode gda_default_connection_open (
	GdaServerConnection *cnc,
	const gchar *cnc_str,
	const gchar *username,
	const gchar *password);
static GNOME_Database_ErrorCode gda_default_connection_close (GdaServerConnection *cnc);

static GdaServerCommand *gda_default_connection_create_command (GdaServerConnection *cnc);

static GNOME_Database_ErrorCode gda_default_connection_begin_transaction (
	GdaServerConnection *cnc, const gchar *name);
static GNOME_Database_ErrorCode gda_default_connection_commit_transaction (
	GdaServerConnection *cnc, const gchar *name);
static GNOME_Database_ErrorCode gda_default_connection_rollback_transaction (
	GdaServerConnection *cnc, const gchar *name);

static GdaServerConnectionClass *parent_class = NULL;

/*
 * GdaDefaultConnection class implementation
 */
static void
gda_default_connection_class_init (GdaDefaultConnectionClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;
	GdaServerConnectionClass *server_class = (GdaServerConnectionClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_default_connection_finalize;

	server_class->open = gda_default_connection_open;
	server_class->close = gda_default_connection_close;
	server_class->create_command = gda_default_connection_create_command;
	server_class->begin_transaction = gda_default_connection_begin_transaction;
	server_class->commit_transaction = gda_default_connection_commit_transaction;
	server_class->rollback_transaction = gda_default_connection_rollback_transaction;
}

static void
gda_default_connection_instance_init (GObject *object, GTypeClass *klass)
{
	GdaDefaultConnection *default_cnc = GDA_DEFAULT_CONNECTION (object);
}

static void
gda_default_connection_finalize (GObject *object)
{
	GdaDefaultConnection *default_cnc = GDA_DEFAULT_CONNECTION (object);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_default_connection_get_type
 */
GType
gda_default_connection_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaDefaultConnectionClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_default_connection_class_init,
			NULL,
			NULL,
			sizeof (GdaDefaultConnection),
			0,
			(GInstanceInitFunc) gda_default_connection_instance_init
		};
		type = g_type_register_static (gda_server_connection_get_type (),
					       "GdaDefaultConnection", &info, 0);
	}

	return type;
}

/* open handler for the default provider */
static GNOME_Database_ErrorCode
gda_default_connection_open (GdaServerConnection *cnc,
			     const gchar *cnc_str,
			     const gchar *username,
			     const gchar *password)
{
	gchar *p_dir = NULL;
	GdaQuarkList *param_list;
	gchar *errmsg = NULL;
	GNOME_Database_ErrorCode retval;
	GdaDefaultConnection *default_cnc = (GdaDefaultConnection *) cnc;

	g_return_val_if_fail (GDA_IS_DEFAULT_CONNECTION (default_cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (cnc_str != NULL, GNOME_Database_ERROR_MISSING_PARAMETER);
	g_return_val_if_fail (default_cnc->sqlite == NULL, GNOME_Database_ERROR_ALREADY_OPEN);

	/* parse the connection string */
	param_list = gda_quark_list_new_from_string (cnc_str);
	p_dir = (gchar *) gda_quark_list_find (param_list, "DIRECTORY");
	if (!p_dir) {
		gda_quark_list_free (param_list);
		gda_server_connection_add_error_string (cnc,
							GNOME_Database_ERROR_PROVIDER_ERROR,
							_("You must specifiy a directory for the database to be stored"));
		return GNOME_Database_ERROR_PROVIDER_ERROR;
	}

	/* open the given database */
	default_cnc->sqlite = sqlite_open (p_dir, 0666, &errmsg);
	if (!default_cnc->sqlite) {
		gda_server_connection_add_error_string (cnc,
							GNOME_Database_ERROR_PROVIDER_ERROR,
							errmsg);
		retval = GNOME_Database_ERROR_PROVIDER_ERROR;
	}
	else
		retval = GNOME_Database_ERROR_SUCCESS;

	/* free memory */
	if (errmsg)
		free (errmsg);

	return retval;
}

/* close handler for the default provider */
static GNOME_Database_ErrorCode
gda_default_connection_close (GdaServerConnection *cnc)
{
	GdaDefaultConnection *default_cnc = (GdaDefaultConnection *) cnc;

	g_return_val_if_fail (GDA_IS_DEFAULT_CONNECTION (default_cnc), GNOME_Database_ERROR_INVALID_OBJECT);

	if (default_cnc->sqlite != NULL) {
		sqlite_close (default_cnc->sqlite);
		default_cnc->sqlite = NULL;
	}

	return GNOME_Database_ERROR_SUCCESS;
}

/* create_command handler for the default provider */
static GdaServerCommand *
gda_default_connection_create_command (GdaServerConnection *cnc)
{
	return gda_default_command_new (cnc);
}

/* begin_transaction handler for the default provider */
static GNOME_Database_ErrorCode
gda_default_connection_begin_transaction (GdaServerConnection *cnc,
					  const gchar *name)
{
	gchar *errmsg = NULL;
	GNOME_Database_ErrorCode retval;
	GdaDefaultConnection *default_cnc = (GdaDefaultConnection *) cnc;

	g_return_val_if_fail (GDA_IS_DEFAULT_CONNECTION (default_cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (default_cnc->sqlite != NULL, GNOME_Database_ERROR_INVALID_OPERATION);

	if (sqlite_exec (default_cnc->sqlite, "BEGIN", NULL, NULL, &errmsg) == SQLITE_OK)
		retval = GNOME_Database_ERROR_SUCCESS;
	else {
		gda_server_connection_add_error_string (cnc,
							GNOME_Database_ERROR_PROVIDER_ERROR,
							errmsg);
		retval = GNOME_Database_ERROR_PROVIDER_ERROR;
	}

	if (errmsg)
		free (errmsg);

	return retval;
}

/* commit_transaction handler for the default provider */
static GNOME_Database_ErrorCode
gda_default_connection_commit_transaction (GdaServerConnection *cnc,
					   const gchar *name)
{
	gchar *errmsg = NULL;
	GNOME_Database_ErrorCode retval;
	GdaDefaultConnection *default_cnc = (GdaDefaultConnection *) cnc;

	g_return_val_if_fail (GDA_IS_DEFAULT_CONNECTION (default_cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (default_cnc->sqlite != NULL, GNOME_Database_ERROR_INVALID_OPERATION);

	if (sqlite_exec (default_cnc->sqlite, "COMMIT", NULL, NULL, &errmsg) == SQLITE_OK)
		retval = GNOME_Database_ERROR_SUCCESS;
	else {
		gda_server_connection_add_error_string (cnc,
							GNOME_Database_ERROR_PROVIDER_ERROR,
							errmsg);
		retval = GNOME_Database_ERROR_PROVIDER_ERROR;
	}

	if (errmsg)
		free (errmsg);

	return retval;
}

/* rollback_transaction handler for the default provider */
static GNOME_Database_ErrorCode
gda_default_connection_rollback_transaction (GdaServerConnection *cnc,
					     const gchar *name)
{
	gchar *errmsg = NULL;
	GNOME_Database_ErrorCode retval;
	GdaDefaultConnection *default_cnc = (GdaDefaultConnection *) cnc;

	g_return_val_if_fail (GDA_IS_DEFAULT_CONNECTION (default_cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (default_cnc->sqlite != NULL, GNOME_Database_ERROR_INVALID_OPERATION);

	if (sqlite_exec (default_cnc->sqlite, "ROLLBACK", NULL, NULL, &errmsg) == SQLITE_OK)
		retval = GNOME_Database_ERROR_SUCCESS;
	else {
		gda_server_connection_add_error_string (cnc,
							GNOME_Database_ERROR_PROVIDER_ERROR,
							errmsg);
		retval = GNOME_Database_ERROR_PROVIDER_ERROR;
	}

	if (errmsg)
		free (errmsg);

	return retval;
}
