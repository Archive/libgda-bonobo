/* GDA Default provider
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Library; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-default-command.h"

/*
 * Callbacks
 */
static GdaServerRecordset *
command_exec_func (GdaServerCommand *cmd, gpointer user_data)
{
	GdaServerRecordset *recset = NULL;

	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), NULL);

	return recset;
}

/*
 * Public functions
 */
GdaServerCommand *
gda_default_command_new (GdaServerConnection *cnc)
{
	GdaServerCommand *cmd = NULL;

	/* create the command instance */
	cmd = gda_server_command_new (cnc, command_exec_func, NULL);
	if (GDA_IS_SERVER_COMMAND (cmd)) {
	}

	return cmd;
}
