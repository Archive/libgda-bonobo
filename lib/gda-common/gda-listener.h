/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_listener_h__)
#  define __gda_listener_h__

#include <glib/gmacros.h>
#include <glib-object.h>
#include <bonobo/bonobo-xobject.h>
#include <GNOME_Database.h>

G_BEGIN_DECLS

#define GDA_LISTENER_TYPE        (gda_listener_get_type ())
#define GDA_LISTENER(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_LISTENER_TYPE, GdaListener))
#define GDA_LISTENER_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_LISTENER_TYPE, GdaListenerClass))
#define GDA_IS_LISTENER(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_LISTENER_TYPE))
#define GDA_IS_LISTENER_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_LISTENER_TYPE))

typedef struct _GdaListener        GdaListener;
typedef struct _GdaListenerClass   GdaListenerClass;
typedef struct _GdaListenerPrivate GdaListenerPrivate;

struct _GdaListener {
	BonoboXObject object;
	GdaListenerPrivate *priv;
};

struct _GdaListenerClass {
	BonoboXObjectClass parent_class;

	/* signals */
	void (* notify_action) (GdaListener *listener,
				GNOME_Database_ListenerAction action,
				const gchar *message,
				const gchar *description);
};

GType        gda_listener_get_type (void);
GdaListener *gda_listener_new (void);
void         gda_listener_free (GdaListener *listener);

G_END_DECLS

#endif
