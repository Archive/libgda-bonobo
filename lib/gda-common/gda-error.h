/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_error_h__)
#  define __gda_error_h__

#include <glib/gmacros.h>
#include <GNOME_Database.h>

G_BEGIN_DECLS

typedef GNOME_Database_Error GdaError;
typedef GNOME_Database_ErrorSeq GdaErrorList;

GdaError                *gda_error_new (void);
void                     gda_error_free (GdaError *error);

GNOME_Database_ErrorCode gda_error_get_code (GdaError *error);
void                     gda_error_set_code (GdaError *error,
					     GNOME_Database_ErrorCode code);
const gchar             *gda_error_get_description (GdaError *error);
void                     gda_error_set_description (GdaError *error,
						    const gchar *description);
const gchar             *gda_error_get_source (GdaError *error);
void                     gda_error_set_source (GdaError *error, const gchar *source);

void                     gda_error_list_free (GdaErrorList *error_list);

G_END_DECLS

#endif
