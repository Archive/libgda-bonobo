/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_row_h__)
#  define __gda_row_h__

#include <glib/gmacros.h>
#include <GNOME_Database.h>
#include <gda-field.h>

G_BEGIN_DECLS

typedef GNOME_Database_Row GdaRow;

GdaRow   *gda_row_new (gint cols);
void      gda_row_free (GdaRow *row);

GdaField *gda_row_get_field_by_pos (GdaRow *row, gint pos);
GdaField *gda_row_get_field_by_name (GdaRow *row, const gchar *name);
gint      gda_row_get_field_count (GdaRow *row);

G_END_DECLS

#endif
