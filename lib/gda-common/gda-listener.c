/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-common-decls.h"
#include "gda-listener.h"

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

static void gda_listener_class_init    (GdaListenerClass *klass);
static void gda_listener_instance_init (GObject *object, GTypeClass *klass);
static void gda_listener_finalize      (GObject *object);

struct _GdaListenerPrivate {
};

static BonoboObjectClass *parent_class = NULL;

/*
 * GNOME::Database::Listener CORBA interface methods
 */
void
impl_GNOME_Database_Listener_notifyAction (PortableServer_Servant servant,
					   GNOME_Database_ListenerAction action,
					   const CORBA_char *message,
					   const CORBA_char *description,
					   CORBA_Environment *ev)
{
	GdaListener = (GdaListener *) bonobo_x_object (servant);

	g_signal_emit_by_name (G_OBJECT (listener),
			       "notify_action",
			       0,
			       action,
			       message,
			       description);
}

/*
 * GdaListener class implementation
 */
static void
gda_listener_class_init (GdaListenerClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;
	POA_GNOME_Database_Listener__epv *epv;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_listener_finalize;

	/* set the epv */
	epv = &klass->epv;
	epv->notifyAction = impl_GNOME_Database_Listener_notifyAction;
}

static void
gda_listener_instance_init (GObject *object, GTypeClass *klass)
{
	GdaListener *listener = GDA_LISTENER (object);

	listener->priv = g_new0 (GdaListenerPrivate, 1);
}

static void
gda_listener_finalize (GObject *object)
{
	GdaListener *listener = GDA_LISTENER (object);

	/* free memory */
	g_free (listener->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_listener_get_type
 */
GType
gda_listener_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaListenerClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_listener_class_init,
			NULL,
			NULL,
			sizeof (GdaListener),
			0,
			(GInstanceInitFunc) gda_listener_instance_init
		};
		type = bonobo_x_type_unique (
			PARENT_TYPE,
			POA_GNOME_Database_Listener__init,
			NULL,
			G_STRUCT_OFFSET (GdaListenerClass, epv),
			&info,
			"GdaListener");
	}

	return type;
}

/**
 * gda_listener_new
 */
GdaListener *
gda_listener_new (void)
{
	GdaListener *listener;

	listener = GDA_LISTENER (g_object_new (GDA_LISTENER_TYPE, NULL));
	return listener;
}

/**
 * gda_listener_free
 */
void
gda_listener_free (GdaListener *listener)
{
	bonobo_object_unref (BONOBO_OBJECT (listener));
}
