/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include "gda-common-decls.h"
#include "gda-config.h"

/* GConf declares some types in gconf/gconf-glib-public.h that are
 * already defined in glib.h, if we use glib 1.3. The next 3 lines
 * are needed to prevent this. */
#ifdef HAVE_GOBJECT
#  define GCONF_GCONF_GLIB_PUBLIC_H
#endif
#include <gconf/gconf.h>

struct _GdaDsn {
	gchar *gda_name;
	gchar *provider;
	gchar *dsn_str;
	gchar *description;
	gchar *username;
};

static GConfEngine *conf_engine = NULL;

static GConfEngine *
get_conf_engine (void)
{
	if (!conf_engine) {
		/* initialize GConf */
		if (!gconf_is_initialized ())
			gconf_init (0, NULL, NULL);
		conf_engine = gconf_engine_get_default ();
	}
	return conf_engine;
}

/**
 * gda_config_get_string
 * @path: path to the configuration entry
 *
 * Gets the value of the specified configuration entry as a string. You
 * are then responsible to free the returned string
 *
 * Returns: the value stored at the given entry
 */
gchar *
gda_config_get_string (const gchar *path)
{
        return gconf_engine_get_string (get_conf_engine (), path, NULL);
}

/**
 * gda_config_get_int
 * @path: path to the configuration entry
 *
 * Gets the value of the specified configuration entry as an integer
 *
 * Returns: the value stored at the given entry
 */
gint
gda_config_get_int (const gchar *path)
{
        return gconf_engine_get_int (get_conf_engine (), path, NULL);
}

/**
 * gda_config_get_float
 * @path: path to the configuration entry
 *
 * Gets the value of the specified configuration entry as a float
 *
 * Returns: the value stored at the given entry
 */
gdouble
gda_config_get_float (const gchar *path)
{
        return gconf_engine_get_float (get_conf_engine (), path, NULL);
}

/**
 * gda_config_get_boolean
 * @path: path to the configuration entry
 *
 * Gets the value of the specified configuration entry as a boolean
 *
 * Returns: the value stored at the given entry
 */
gboolean
gda_config_get_boolean (const gchar *path)
{
        return gconf_engine_get_bool (get_conf_engine (), path, NULL);
}

/**
 * gda_config_set_string
 * @path: path to the configuration entry
 * @new_value: new value
 *
 * Sets the given configuration entry to contain a string
 */
void
gda_config_set_string (const gchar *path, const gchar *new_value)
{
        gconf_engine_set_string (get_conf_engine (), path, new_value, NULL);
}

/**
 * gda_config_set_int
 * @path: path to the configuration entry
 * @new_value: new value
 *
 * Sets the given configuration entry to contain an integer
 */
void
gda_config_set_int (const gchar *path, gint new_value)
{
        gconf_engine_set_int (get_conf_engine (), path, new_value, NULL);
}

/**
 * gda_config_set_float
 * @path: path to the configuration entry
 * @new_value: new value
 *
 * Sets the given configuration entry to contain a float
 */
void
gda_config_set_float (const gchar *path, gdouble new_value)
{
        gconf_engine_set_float (get_conf_engine (), path, new_value, NULL);
}

/**
 * gda_config_set_boolean
 * @path: path to the configuration entry
 * @new_value: new value
 *
 * Sets the given configuration entry to contain a boolean
 */
void
gda_config_set_boolean (const gchar *path, gboolean new_value)
{
        g_return_if_fail(path != NULL);
        gconf_engine_set_bool (get_conf_engine (), path, new_value, NULL);
}

/**
 * gda_config_remove_section
 * @path: path to the configuration section
 *
 * Remove the given section from the configuration database
 */
void
gda_config_remove_section (const gchar *path)
{
        g_return_if_fail(path != NULL);
        gconf_engine_remove_dir (get_conf_engine (), path, NULL);
}
/**
 * gda_config_remove_key
 * @path: path to the configuration entry
 *
 * Remove the given entry from the configuration database
 */
void
gda_config_remove_key (const gchar *path)
{
        gconf_engine_unset (get_conf_engine (), path, NULL);
}

/**
 * gda_config_has_section
 * @path: path to the configuration section
 *
 * Checks whether the given section exists in the configuration
 * system
 *
 * Returns: TRUE if the section exists, FALSE otherwise
 */
gboolean
gda_config_has_section (const gchar *path)
{
        return gconf_engine_dir_exists (get_conf_engine (), path, NULL);
}

/**
 * gda_config_has_key
 * @path: path to the configuration key
 *
 * Check whether the given key exists in the configuration system
 *
 * Returns: TRUE if the entry exists, FALSE otherwise
 */
gboolean
gda_config_has_key (const gchar *path)
{
        GConfValue* value;

        g_return_val_if_fail(path != NULL, FALSE);

        value = gconf_engine_get (get_conf_engine (), path, NULL);
        if (value) {
                gconf_value_free (value);
                return TRUE;
        }
        return FALSE;
}

/**
 * gda_config_list_sections
 * @path: path for root dir
 *
 * Return a GList containing the names of all the sections available
 * under the given root directory.
 *
 * To free the returned value, you can use #gda_config_free_list
 *
 * Returns: a list containing all the section names
 */
GList *
gda_config_list_sections (const gchar *path)
{
        GList *ret = NULL;
        GSList *slist;

        g_return_val_if_fail(path != NULL, NULL);

        slist = gconf_engine_all_dirs (get_conf_engine (), path, NULL);
        if (slist) {
                GSList *node;

                for (node = slist; node != NULL; node = g_slist_next (node)) {
                        gchar *section_name = strrchr ((const char *) node->data, '/');
                        if (section_name) {
				gchar *tmp = g_strdup (section_name + 1);
                                ret = g_list_append(ret, tmp);
                        }
		}
                g_slist_free (slist);
        }
        return ret;
}
/**
 * gda_config_list_keys
 * @path: path for root dir
 *
 * Returns a list of all keys that exist under the given path.
 *
 * To free the returned value, you can use #gda_config_free_list
 *
 * Returns: a list containing all the key names
 */
GList *
gda_config_list_keys (const gchar *path)
{
        GList *ret = NULL;
        GSList *slist;

        g_return_val_if_fail (path != NULL, NULL);

        slist = gconf_engine_all_entries (get_conf_engine (), path, NULL);
        if (slist) {
                GSList *node;

                for (node = slist; node != NULL; node = g_slist_next (node)) {
                        GConfEntry *entry = (GConfEntry *) node->data;
                        if (entry) {
                                gchar *entry_name;

                                entry_name = strrchr ((const char *) gconf_entry_get_key (entry), '/');
                                if (entry_name) {
					gchar *tmp = g_strdup(entry_name + 1);
					ret = g_list_append(ret, tmp);
                                }
                                gconf_entry_free (entry);
                        }
                }
                g_slist_free (slist);
        }
        return ret;
}

/**
 * gda_config_free_list
 * @list: list to be freed
 *
 * Free all memory used by the given GList, which must be the return value
 * from either #gda_config_list_sections and #gda_config_list_keys
 */
void
gda_config_free_list (GList *list)
{
        while (list != NULL) {
                gchar* str = (gchar *) list->data;
                list = g_list_remove (list, (gpointer) str);
                g_free (str);
        }
}

/**
 * gda_dsn_new
 */
GdaDsn *
gda_dsn_new (void)
{
	GdaDsn *dsn = g_new0 (GdaDsn, 1);
	return dsn;
}

/**
 * gda_dsn_free
 */
void
gda_dsn_free (GdaDsn *dsn)
{
	g_return_if_fail (dsn != NULL);

	if (dsn->gda_name)
		g_free (dsn->gda_name);
	if (dsn->provider)
		g_free (dsn->provider);
	if (dsn->dsn_str)
		g_free (dsn->dsn_str);
	if (dsn->description)
		g_free (dsn->description);
	if (dsn->username)
		g_free (dsn->username);

	g_free (dsn);
}

/**
 * gda_dsn_copy
 */
GdaDsn *
gda_dsn_copy (GdaDsn *dsn)
{
	GdaDsn *new_dsn;

	g_return_val_if_fail (dsn != NULL, NULL);

	new_dsn = gda_dsn_new ();
	gda_dsn_set_name (new_dsn, dsn->gda_name);
	gda_dsn_set_provider (new_dsn, dsn->provider);
	gda_dsn_set_string (new_dsn, dsn->dsn_str);
	gda_dsn_set_description (new_dsn, dsn->description);
	gda_dsn_set_username (new_dsn, dsn->username);

	return new_dsn;
}

/**
 * gda_dsn_get_name
 */
const gchar *
gda_dsn_get_name (GdaDsn *dsn)
{
	g_return_val_if_fail (dsn != NULL, NULL);
	return (const gchar *) dsn->gda_name;
}

/**
 * gda_dsn_set_name
 */
void
gda_dsn_set_name (GdaDsn *dsn, const gchar *name)
{
	g_return_if_fail (dsn != NULL);

	if (dsn->gda_name)
		g_free (dsn->gda_name);
	dsn->gda_name = g_strdup (name);
}

/**
 * gda_dsn_get_provider
 */
const gchar *
gda_dsn_get_provider (GdaDsn *dsn)
{
	g_return_val_if_fail (dsn != NULL, NULL);
	return (const gchar *) dsn->provider;
}

/**
 * gda_dsn_set_provider
 */
void
gda_dsn_set_provider (GdaDsn *dsn, const gchar *provider)
{
	g_return_if_fail (dsn != NULL);

	if (dsn->provider)
		g_free (dsn->provider);
	dsn->provider = g_strdup (provider);
}

/**
 * gda_dsn_get_string
 */
const gchar *
gda_dsn_get_string (GdaDsn *dsn)
{
	g_return_val_if_fail (dsn != NULL, NULL);
	return (const gchar *) dsn->dsn_str;
}

/**
 * gda_dsn_set_string
 */
void
gda_dsn_set_string (GdaDsn *dsn, const gchar *string)
{
	g_return_if_fail (dsn != NULL);

	if (dsn->dsn_str)
		g_free (dsn->dsn_str);
	dsn->dsn_str = g_strdup (string);
}

/**
 * gda_dsn_get_description
 */
const gchar *
gda_dsn_get_description (GdaDsn *dsn)
{
	g_return_val_if_fail (dsn != NULL, NULL);
	return (const gchar *) dsn->description;
}

/**
 * gda_dsn_set_description
 */
void
gda_dsn_set_description (GdaDsn *dsn, const gchar *description)
{
	g_return_if_fail (dsn != NULL);

	if (dsn->description)
		g_free (dsn->description);
	dsn->description = g_strdup (description);
}

/**
 * gda_dsn_get_username
 */
const gchar *
gda_dsn_get_username (GdaDsn *dsn)
{
	g_return_val_if_fail (dsn != NULL, NULL);
	return (const gchar *) dsn->username;
}

/**
 * gda_dsn_set_username
 */
void
gda_dsn_set_username (GdaDsn *dsn, const gchar *username)
{
	g_return_if_fail (dsn != NULL);

	if (dsn->username)
		g_free (dsn->username);
	dsn->username = g_strdup (username);
}

/**
 * gda_dsn_save
 */
gboolean
gda_dsn_save (GdaDsn *dsn)
{
}

/**
 * gda_dsn_remove
 */
gboolean
gda_dsn_remove (GdaDsn *dsn)
{
}

/**
 * gda_dsn_find_by_name
 */
GdaDsn *
gda_dsn_find_by_name (const gchar *name)
{
	GdaDsn *dsn = NULL;
	GList *dsn_list;
	GList *l;

	g_return_val_if_fail (name != NULL, NULL);

	dsn_list = gda_dsn_list ();
	for (l = dsn_list; l != NULL; l = g_list_next (l)) {
		const gchar *dsn_name = gda_dsn_get_name ((GdaDsn *) l->data);
		if (dsn_name && !strcmp (dsn_name, name)) {
			dsn = gda_dsn_copy ((GdaDsn *) l->data);
			break;
		}
	}
	gda_dsn_list_free (dsn_list);

	return dsn;
}

static gchar *
get_config_string (const gchar *format, ...)
{
	static gchar *ptr = NULL;
        gchar buffer[2048];
        va_list args;

        g_return_val_if_fail(format, 0);

        va_start(args, format);
        vsprintf(buffer, format, args);
        va_end(args);

	if (ptr != NULL)
		g_free (ptr);
        ptr = gda_config_get_string(buffer);
	return ptr;
}

/**
 * gda_dsn_list
 */
GList *
gda_dsn_list (void)
{
	GList* datasources = NULL;
        GList* ret = NULL;
        GList* node;

        datasources = gda_config_list_sections (GDA_CONFIG_SECTION_DATASOURCES);
        for (node = datasources; node != NULL; node = g_list_next (node)) {
                GdaDsn *dsn;
                gchar *name;
   
                name = (gchar *) node->data;
                if (name) {
                        dsn = gda_dsn_new ();
                        gda_dsn_set_name (dsn, name);
                        gda_dsn_set_provider (
				dsn,
				get_config_string ("%s/%s/Provider",
						   GDA_CONFIG_SECTION_DATASOURCES,
						   name));
                        gda_dsn_set_string (
				dsn,
				get_config_string ("%s/%s/DSN",
						   GDA_CONFIG_SECTION_DATASOURCES,
						   name));
                        gda_dsn_set_description (
				dsn,
				get_config_string ("%s/%s/Description",
						   GDA_CONFIG_SECTION_DATASOURCES,
						   name));
                        gda_dsn_set_username (
				dsn,
				get_config_string ("%s/%s/Username",
						   GDA_CONFIG_SECTION_DATASOURCES,
						   name));

			/* add datasource to return list */
                        ret = g_list_append (ret, (gpointer) dsn);
                }
        }
        gda_config_free_list (datasources);
 
        return ret;
}

/**
 * gda_dsn_list_for_provider
 */
GList *
gda_dsn_list_for_provider (const gchar *provider)
{
	GList *dsn_list;
	GList *retval = NULL;
	GList *l;

	g_return_val_if_fail (provider != NULL, NULL);

	dsn_list = gda_dsn_list ();
	for (l = dsn_list; l != NULL; l = g_list_next (l)) {
		const gchar *dsn_provider = gda_dsn_get_provider ((GdaDsn *) l->data);
		if (dsn_provider && !strcmp (dsn_provider, provider)) {
			retval = g_list_append (retval,
						gda_dsn_copy ((GdaDsn *) l->data));
		}
	}
	gda_dsn_list_free (dsn_list);

	return retval;
}

/**
 * gda_dsn_list_free
 */
void
gda_dsn_list_free (GList *dsn_list)
{
	GList *l;

	while ((l = g_list_first (dsn_list))) {
		GdaDsn *dsn = (GdaDsn *) l->data;
		dsn_list = g_list_remove (dsn_list, dsn);
		gda_dsn_free (dsn);
	}
}
