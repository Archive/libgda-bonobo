/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-error.h"

/**
 * gda_error_new
 *
 * Creates a new #GdaError object
 *
 * Returns: the newly created object
 */
GdaError *
gda_error_new (void)
{
	return (GdaError *) GNOME_Database_Error__alloc ();
}

/**
 * gda_error_free
 */
void
gda_error_free (GdaError *error)
{
	g_return_if_fail (error != NULL);

	if (error->source)
		CORBA_free (error->source);
	if (error->description)
		CORBA_free (error->description);

	CORBA_free (error);
}

/**
 * gda_error_get_code
 */
GNOME_Database_ErrorCode
gda_error_get_code (GdaError *error)
{
	g_return_val_if_fail (error != NULL, GNOME_Database_ERROR_UNKNOWN);
	return error->code;
}

/**
 * gda_error_set_code
 */
void
gda_error_set_code (GdaError *error, GNOME_Database_ErrorCode code)
{
	g_return_if_fail (error != NULL);
	error->code = code;
}

/**
 * gda_error_get_description
 */
const gchar *
gda_error_get_description (GdaError *error)
{
	g_return_val_if_fail (error != NULL, NULL);
	return (const gchar *) error->description;
}

/**
 * gda_error_set_description
 */
void
gda_error_set_description (GdaError *error, const gchar *description)
{
	g_return_if_fail (error != NULL);

	if (error->description != NULL)
		CORBA_free (error->description);
	error->description = CORBA_string_dup (description);
}

/**
 * gda_error_get_source
 */
const gchar *
gda_error_get_source (GdaError *error)
{
	g_return_val_if_fail (error != NULL, NULL);
	return (const gchar *) error->source;
}

/**
 * gda_error_set_source
 */
void
gda_error_set_source (GdaError *error, const gchar *source)
{
	g_return_if_fail (error != NULL);

	if (error->source)
		CORBA_free (error->source);
	error->source = CORBA_string_dup (source);
}

/**
 * gda_error_list_free
 */
void
gda_error_list_free (GdaErrorList *error_list)
{
	gint idx;

	g_return_if_fail (error_list != NULL);

	for (idx = 0; idx < error_list->_length; idx++) {
		gda_error_free (&error_list->_buffer[idx]);
	}
	CORBA_free (error_list);
}
