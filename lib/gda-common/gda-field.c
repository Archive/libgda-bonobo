/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-common-decls.h"
#include "gda-field.h"

/**
 * gda_field_new
 *
 * Creates a new #GdaField object
 *
 * Returns: The newly created object
 */
GdaField *
gda_field_new (const gchar *name)
{
	GdaField *field;

	field = (GdaField *) GNOME_Database_Field__alloc ();
	field->name = name ? CORBA_string_dup (name) : CORBA_string_dup ("");
	field->size = 0;
	memset (&field->value, 0, sizeof (field->value));
	field->value._d = GNOME_Database_TYPE_UNKNOWN;

	return field;
}

/**
 * gda_field_free
 */
void
gda_field_free (GdaField *field)
{
	g_return_if_fail (field != NULL);

	if (field->name)
		CORBA_free (field->name);

	CORBA_free (field);
}

/**
 * gda_field_get_value_type
 */
GNOME_Database_ValueType
gda_field_get_value_type (GdaField *field)
{
	g_return_val_if_fail (field != NULL, GNOME_Database_TYPE_NULL);
	return field->value._d;
}

/**
 * gda_field_get_name
 */
const gchar *
gda_field_get_name (GdaField *field)
{
	g_return_val_if_fail (field != NULL, NULL);
	return (const gchar *) field->name;
}

/**
 * gda_field_set_name
 */
void
gda_field_set_name (GdaField *field, const gchar *name)
{
	g_return_if_fail (field != NULL);
	g_return_if_fail (name != NULL);

	if (field->name != NULL)
		CORBA_free (field->name);
	field->name = CORBA_string_dup (name);
}

/**
 * gda_field_get_size
 */
glong
gda_field_get_size (GdaField *field)
{
	g_return_val_if_fail (field != NULL, -1);
	return field->size;
}

/**
 * gda_field_set_size
 */
void
gda_field_set_size (GdaField *field, glong size)
{
	g_return_if_fail (field != NULL);

	field->size = size;
}

/**
 * gda_field_get_bigint_value
 */
glong
gda_field_get_bigint_value (GdaField *field)
{
	return field ? field->value._u.ll : -1;
}

/**
 * gda_field_set_bigint_value
 */
void
gda_field_set_bigint_value (GdaField *field, glong value)
{
	g_return_if_fail (field != NULL);

	field->value._d = GNOME_Database_TYPE_BIGINT;
	field->value._u.ll = value;
}

/**
 * gda_field_get_tinyint_value
 */
gchar
gda_field_get_tinyint_value (GdaField *field)
{
	return field ? field->value._u.c : 0;
}

/**
 * gda_field_set_tinyint_value
 */
void
gda_field_set_tinyint_value (GdaField *field, gchar value)
{
	g_return_if_fail (field != NULL);

	field->value._d = GNOME_Database_TYPE_TINYINT;
	field->value._u.c = value;
}
