/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_config_h__)
#  define __gda_config_h__

#include <glib/gmacros.h>

G_BEGIN_DECLS

#define GDA_CONFIG_SECTION_DATASOURCES "/apps/gda/Datasources"

/*
 * Access to system configuration
 */
gchar   *gda_config_get_string (const gchar *path);
void     gda_config_set_string (const gchar *path, const gchar *new_value);
gint     gda_config_get_int (const gchar *path);
void     gda_config_set_int (const gchar *path, gint new_value);
gdouble  gda_config_get_float (const gchar *path);
void     gda_config_set_float (const gchar *path, gdouble new_value);
gboolean gda_config_get_boolean (const gchar *path);
void     gda_config_set_boolean (const gchar *path, gboolean new_value);

void     gda_config_remove_section (const gchar *path);
void     gda_config_remove_key (const gchar *path);
gboolean gda_config_has_section (const gchar *path);
gboolean gda_config_has_key (const gchar *path);

GList   *gda_config_list_sections (const gchar *path);
GList   *gda_config_list_keys (const gchar *path);
void     gda_config_free_list (GList *list);


/*
 * Data source configuration management
 */
typedef struct _GdaDsn GdaDsn;

GdaDsn *gda_dsn_list_new (void);
void    gda_dsn_free (GdaDsn *dsn);
GdaDsn *gda_dsn_copy (GdaDsn *dsn);

const gchar *gda_dsn_get_name (GdaDsn *dsn);
void         gda_dsn_set_name (GdaDsn *dsn, const gchar *name);
const gchar *gda_dsn_get_provider (GdaDsn *dsn);
void         gda_dsn_set_provider (GdaDsn *dsn, const gchar *provider);
const gchar *gda_dsn_get_string (GdaDsn *dsn);
void         gda_dsn_set_string (GdaDsn *dsn, const gchar *string);
const gchar *gda_dsn_get_description (GdaDsn *dsn);
void         gda_dsn_set_description (GdaDsn *dsn, const gchar *description);
const gchar *gda_dsn_get_username (GdaDsn *dsn);
void         gda_dsn_set_username (GdaDsn *dsn, const gchar *username);

gboolean gda_dsn_save (GdaDsn *dsn);
gboolean gda_dsn_remove (GdaDsn *dsn);

GdaDsn *gda_dsn_find_by_name (const gchar *name);
GList  *gda_dsn_list (void);
GList  *gda_dsn_list_for_provider (const gchar *provider);
void    gda_dsn_list_free (GList *dsn_list);

G_END_DECLS

#endif
