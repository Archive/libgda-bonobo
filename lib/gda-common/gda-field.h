/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_field_h__)
#  define __gda_field_h__

#include <glib/gmacros.h>
#include <GNOME_Database.h>

G_BEGIN_DECLS

typedef GNOME_Database_Field GdaField;

GdaField                *gda_field_new (const gchar *name);
void                     gda_field_free (GdaField *field);

GNOME_Database_ValueType gda_field_get_value_type (GdaField *field);
const gchar             *gda_field_get_name (GdaField *field);
void                     gda_field_set_name (GdaField *field, const gchar *name);
glong                    gda_field_get_size (GdaField *field);
void                     gda_field_set_size (GdaField *field, glong size);

gchar                   *gda_field_stringify (GdaField *field);

glong                    gda_field_get_bigint_value (GdaField *field);
void                     gda_field_set_bigint_value (GdaField *field, glong value);
gpointer                 gda_field_get_binary_value (GdaField *field);
void                     gda_field_set_binary_value (GdaField *field, gpointer value);
gboolean                 gda_field_get_boolean_value (GdaField *field);
void                     gda_field_set_boolean_value (GdaField *field, gboolean value);
/* FIXME: _set/_get_*date* functions */
gdouble                  gda_field_get_double_value (GdaField *field);
void                     gda_field_set_double_value (GdaField *field, gdouble value);
gint                     gda_field_get_int_value (GdaField *field);
void                     gda_field_set_int_value (GdaField *field, gint value);
gfloat                   gda_field_get_single_value (GdaField *field);
void                     gda_field_set_single_value (GdaField *field, gfloat value);
gshort                   gda_field_get_smallint_value (GdaField *field);
void                     gda_field_set_smallint_value (GdaField *field, gshort value);
gchar                   *gda_field_get_string_value (GdaField *field);
void                     gda_field_set_string_value (GdaField *field, gchar *value);
gchar                    gda_field_get_tinyint_value (GdaField *field);
void                     gda_field_set_tinyint_value (GdaField *field, gchar value);
gulong                   gda_field_get_ubigint (GdaField *field);
void                     gda_field_set_ubigint (GdaField *field, gulong value);
gushort                  gda_field_get_usmallint (GdaField *field);
void                     gda_field_set_usmallint (GdaField *field, gushort value);

G_END_DECLS

#endif
