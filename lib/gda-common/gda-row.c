/* GDA Common Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-row.h"

/**
 * gda_row_new
 * @cols: Number of columns in the row
 */
GdaRow *
gda_row_new (gint cols)
{
	GdaRow *row;

	row = (GdaRow *) GNOME_Database_Row__alloc ();
	CORBA_sequence_set_release (row, TRUE);
	row->_length = cols;
	row->_buffer = CORBA_sequence_GNOME_Database_Field_allocbuf (cols);

	return row;
}

/**
 * gda_row_free
 */
void
gda_row_free (GdaRow *row)
{
	g_return_if_fail (row != NULL);

	if (row->_buffer) {
		CORBA_free (row->_buffer);
		row->_buffer = NULL;
	}
	CORBA_free (row);
}

/**
 * gda_row_get_field_by_pos
 */
GdaField *
gda_row_get_field_by_pos (GdaRow *row, gint pos)
{
	g_return_val_if_fail (row != NULL, NULL);
	g_return_val_if_fail (pos < 0, NULL);
	g_return_val_if_fail (pos > row->_length - 1, NULL);

	return (GdaField *) &row->_buffer[pos];
}

/**
 * gda_row_get_field_by_name
 */
GdaField *
gda_row_get_field_by_name (GdaRow *row, const gchar *name)
{
	gint i;

	g_return_val_if_fail (row != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	for (i = 0; i < row->_length; i++) {
		GdaField *field;
		gchar *f_name;

		field = (GdaField *) &row->_buffer[i];
		f_name = gda_field_get_name (field);
		if (f_name && !strcmp (f_name, name))
			return field;
	}

	return NULL; /* not found */
}

/**
 * gda_row_get_field_count
 */
gint
gda_row_get_field_count (GdaRow *row)
{
	g_return_val_if_fail (row != NULL, -1);
	return row->_length;
}
