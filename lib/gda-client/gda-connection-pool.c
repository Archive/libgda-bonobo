/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib/ghash.h>
#include "gda-common-decls.h"
#include "gda-connection-pool.h"

#define PARENT_TYPE G_OBJECT_TYPE

static void gda_connection_pool_class_init    (GdaConnectionPoolClass *klass);
static void gda_connection_pool_instance_init (GObject *object, GTypeClass *klass);
static void gda_connection_pool_finalize      (GObject *object);

struct _GdaConnectionPoolPrivate {
	GHashTable *cnc_list;
};

static GObjectClass *parent_class = NULL;

/*
 * GdaConnectionPool class implementation
 */
static void
gda_connection_pool_class_init (GdaConnectionPoolClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_connection_pool_finalize;
}

static void
gda_connection_pool_instance_init (GObject *object, GTypeClass *klass)
{
	GdaConnectionPool *pool = GDA_CONNECTION_POOL (object);

	pool->priv = g_new0 (GdaConnectionPoolPrivate, 1);
	pool->priv->cnc_list = g_hash_table_new (g_str_hash, g_str_equal);
}

static void
gda_connection_pool_finalize (GObject *object)
{
	GList *l;
	GdaConnectionPool *pool = GDA_CONNECTION_POOL (object);

	/* free memory */
	/* FIXME: free GHashTable */
	g_free (pool->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_connection_pool_get_type
 */
GType
gda_connection_pool_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaConnectionPoolClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_connection_pool_class_init,
			NULL,
			NULL,
			sizeof (GdaConnection),
			0,
			(GInstanceInitFunc) gda_connection_pool_instance_init
		};
		type = g_type_register_static (G_TYPE_OBJECT, "GdaConnectionPool", &info, 0);
	}
	return type;
}

/**
 * gda_connection_pool_new
 *
 * Creates a connection pool, which can be used to share connections
 * between different clients, thus having only one open connection
 * per dsn/username/password combination.
 *
 * When using connection pools, you shouldn't close directly the
 * connections yourself, but have the #GdaConnectionPool do it
 * on its own when calling #gda_connection_pool_close_connection.
 *
 * Returns: a newly created #GdaConnectionPool object
 */
GdaConnectionPool *
gda_connection_pool_new (void)
{
	GdaConnectionPool *pool;

	pool = GDA_CONNECTION_POOL (g_object_new (GDA_CONNECTION_POOL_TYPE, NULL));
	return pool;
}

/**
 * gda_connection_pool_free
 */
void
gda_connection_pool_free (GdaConnectionPool *pool)
{
	g_object_unref (G_OBJECT (pool));
}

/**
 * gda_connection_pool_open_connection
 * @pool: A #GdaConnectionPool object.
 * @dsn: Data source name, as in the GDA configuration.
 * @username: User name to use to connect.
 * @password: User password.
 *
 * Open a connection in the given #GdaConnectionPool.
 *
 * Returns: A #GdaConnection object
 */
GdaConnection *
gda_connection_pool_open_connection (GdaConnectionPool *pool,
				     const gchar *dsn,
				     const gchar *username,
				     const gchar *password)
{
	gchar *str;
	GdaConnection *cnc;

	g_return_val_if_fail (GDA_IS_CONNECTION_POOL (pool), NULL);

	str = g_strdup_printf ("%s@@%s@@%s", dsn, username, password);
	cnc = g_hash_table_lookup (pool->priv->cnc_list, str);
	if (GDA_IS_CONNECTION (cnc)) {
		g_object_ref (G_OBJECT (cnc));
		g_free (str);

		return cnc;
	}

	/* not found, so open a new connection */
	cnc = gda_connection_new_from_dsn (dsn, username, password);
	if (GDA_IS_CONNECTION (cnc)) {
		g_hash_table_insert (pool->priv->cnc_list, str, cnc);
		/* FIXME: connect to "destroy" signal so that we can remove it
		   from our list when it gets destroyed */
	}
	else
		g_free (str);

	return cnc;
}

/**
 * gda_connection_pool_close_connection
 */
void
gda_connection_pool_close_connection (GdaConnectionPool *pool, GdaConnection *cnc)
{
	g_object_unref (G_OBJECT (cnc));
}
