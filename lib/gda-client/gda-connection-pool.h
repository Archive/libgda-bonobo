/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_connection_pool_h__)
#  define __gda_connection_pool_h__

#include <glib/gmacros.h>
#include <glib-object.h>
#include <gda-connection.h>

G_BEGIN_DECLS

#define GDA_CONNECTION_POOL_TYPE        (gda_connection_pool_get_type ())
#define GDA_CONNECTION_POOL(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_CONNECTION_POOL_TYPE, GdaConnectionPool))
#define GDA_CONNECTION_POOL_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_CONNECTION_POOL_TYPE, GdaConnectionPoolClass))
#define GDA_IS_CONNECTION_POOL(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_CONNECTION_POOL_TYPE))
#define GDA_IS_CONNECTION_POOL_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_CONNECTION_POOL_TYPE))

typedef struct _GdaConnectionPool        GdaConnectionPool;
typedef struct _GdaConnectionPoolClass   GdaConnectionPoolClass;
typedef struct _GdaConnectionPoolPrivate GdaConnectionPoolPrivate;

struct _GdaConnectionPool {
	GObject object;
	GdaConnectionPoolPrivate *priv;
};

struct _GdaConnectionPoolClass {
	GObjectClass parent_class;
};

GType              gda_connection_pool_get_type (void);
GdaConnectionPool *gda_connection_pool_new (void);
void               gda_connection_pool_free (GdaConnectionPool *pool);

GdaConnection     *gda_connection_pool_open_connection (GdaConnectionPool *pool,
							const gchar *dsn,
							const gchar *username,
							const gchar *password);
void               gda_connection_pool_close_connection (GdaConnectionPool *pool,
							 GdaConnection *cnc);

G_END_DECLS

#endif
