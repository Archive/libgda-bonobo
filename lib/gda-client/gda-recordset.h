/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_recordset_h__)
#  define __gda_recordset_h__

#include <glib-object.h>
#include <GNOME_Database.h>
#include <gda-row.h>

typedef struct _GdaRecordset GdaRecordset;

#include <gda-connection.h>

G_BEGIN_DECLS

#define GDA_RECORDSET_TYPE        (gda_recordset_get_type ())
#define GDA_RECORDSET(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_RECORDSET_TYPE, GdaRecordset))
#define GDA_RECORDSET_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_RECORDSET_TYPE, GdaRecordsetClass))
#define GDA_IS_RECORDSET(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_RECORDSET_TYPE))
#define GDA_IS_RECORDSET_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_RECORDSET_TYPE))

typedef struct _GdaRecordsetClass   GdaRecordsetClass;
typedef struct _GdaRecordsetPrivate GdaRecordsetPrivate;

struct _GdaRecordset {
	GObject object;
	GdaRecordsetPrivate *priv;
};

struct _GdaRecordsetClass {
	GObjectClass parent_class;
};

GType          gda_recordset_get_type (void);
GdaRecordset  *gda_recordset_new (GdaConnection *cnc,
				 GNOME_Database_Recordset corba_recset);
void           gda_recordset_free (GdaRecordset *recset);

GdaConnection *gda_recordset_get_connection (GdaRecordset *recset);

GdaRow        *gda_recordset_fetch (GdaRecordset *recset);
gboolean       gda_recordset_is_eof (GdaRecordset *recset);

G_END_DECLS

#endif
