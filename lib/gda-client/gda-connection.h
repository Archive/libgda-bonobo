/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_connection_h__)
#  define __gda_connection_h__

#include <glib/gmacros.h>
#include <glib-object.h>
#include <GNOME_Database.h>
#include <gda-error.h>
#include <gda-listener.h>

G_BEGIN_DECLS

#define GDA_CONNECTION_TYPE        (gda_connection_get_type ())
#define GDA_CONNECTION(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_CONNECTION_TYPE, GdaConnection))
#define GDA_CONNECTION_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_CONNECTION_TYPE, GdaConnectionClass))
#define GDA_IS_CONNECTION(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_CONNECTION_TYPE))
#define GDA_IS_CONNECTION_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_CONNECTION_TYPE))

typedef struct _GdaConnection        GdaConnection;
typedef struct _GdaConnectionClass   GdaConnectionClass;
typedef struct _GdaConnectionPrivate GdaConnectionPrivate;

struct _GdaConnection {
	GObject object;
	GdaConnectionPrivate *priv;
};

struct _GdaConnectionClass {
	GObjectClass parent_class;

	/* signals */
	void (* error) (GdaConnection *cnc, const GList *error_list);
};

typedef enum {
	GDA_CONNECTION_FLAGS_EXISTING_ONLY
} GdaConnectionFlags;

GType          gda_connection_get_type (void);
GdaConnection *gda_connection_new (const gchar *id, GdaConnectionFlags flags);
GdaConnection *gda_connection_new_from_dsn (const gchar *dsn_name,
					    const gchar *username,
					    const gchar *password);
void           gda_connection_free (GdaConnection *cnc);

/* Connection error management */
gboolean gda_connection_check_exception (GdaConnection *cnc, CORBA_Environment *ev);
void     gda_connection_add_error (GdaConnection *cnc, GdaError *error);
void     gda_connection_add_error_list (GdaConnection *cnc, GList *error_list);

/* Basic operations */
gchar                   *gda_connection_get_version (GdaConnection *cnc);
Bonobo_Unknown           gda_connection_get_corba_object (GdaConnection *cnc);

GNOME_Database_ErrorCode gda_connection_open (GdaConnection *cnc,
					      const gchar *cnc_str,
					      const gchar *username,
					      const gchar *password);
const gchar             *gda_connection_get_string (GdaConnection *cnc);
const gchar             *gda_connection_get_username (GdaConnection *cnc);
const gchar             *gda_connection_get_password (GdaConnection *cnc);
gboolean                 gda_connection_is_open (GdaConnection *cnc);
GNOME_Database_ErrorCode gda_connection_close (GdaConnection *cnc);

/* Transaction execution */
GNOME_Database_ErrorCode gda_connection_begin_transaction (
	GdaConnection *cnc, const gchar *name);
GNOME_Database_ErrorCode gda_connection_commit_transaction (
	GdaConnection *cnc, const gchar *name);
GNOME_Database_ErrorCode gda_connection_rollback_transaction (
	GdaConnection *cnc, const gchar *name);

/* Database server logging */
GdaListener             *gda_connection_start_logging (GdaConnection *cnc);
GNOME_Database_ErrorCode gda_connection_stop_logging (GdaConnection *cnc,
						      GdaListener *listener);

G_END_DECLS

#endif
