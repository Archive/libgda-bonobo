/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-command.h"
#include "gda-common-decls.h"

#define PARENT_TYPE G_OBJECT_TYPE

static void gda_command_class_init    (GdaCommandClass *klass);
static void gda_command_instance_init (GObject *object, GTypeClass *klass);
static void gda_command_finalize      (GObject *object);

struct _GdaCommandPrivate {
	GdaConnection *cnc;
	GNOME_Database_Command corba_command;
};

static GObjectClass *parent_class = NULL;

/*
 * GdaCommand class implementation
 */
static void
gda_command_class_init (GdaCommandClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_command_finalize;
}

static void
gda_command_instance_init (GObject *object, GTypeClass *klass)
{
	GdaCommand *cmd = GDA_COMMAND (object);

	cmd->priv = g_new0 (GdaCommandPrivate, 1);
}

static void
gda_command_finalize (GObject *object)
{
	GdaCommand *cmd = GDA_COMMAND (object);

	/* free memory */
	if (cmd->priv->corba_command != CORBA_OBJECT_NIL) {
		CORBA_Environment ev;

		CORBA_exception_init (&ev);
		CORBA_Object_release (cmd->priv->corba_command, &ev);
		CORBA_exception_free (&ev);
		cmd->priv->corba_command = CORBA_OBJECT_NIL;
	}

	g_free (cmd->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_command_get_type
 */
GType
gda_command_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaCommandClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_command_class_init,
			NULL,
			NULL,
			sizeof (GdaCommand),
			0,
			(GInstanceInitFunc) gda_command_instance_init
		};
		type = g_type_register_static (G_TYPE_OBJECT, "GdaCommand", &info, 0);
	}
	return type;
}

/**
 * gda_command_new
 */
GdaCommand *
gda_command_new (GdaConnection *cnc)
{
	GNOME_Database_Connection corba_cnc;
	GNOME_Database_Command corba_cmd;
	GdaCommand *cmd = NULL;
	CORBA_Environment ev;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);

	CORBA_exception_init (&ev);

	/* call the createCommand CORBA method */
	corba_cnc = gda_connection_get_corba_object (cnc);
	if (corba_cnc == NULL)
		return NULL;

	corba_cmd = GNOME_Database_Connection_createCommand (corba_cnc, &ev);
	if (corba_cmd != CORBA_OBJECT_NIL &&
	    gda_connection_check_exception (cnc, &ev)) {
		cmd = GDA_COMMAND (g_object_new (GDA_COMMAND_TYPE, NULL));
		cmd->priv->cnc = cnc;
		cmd->priv->corba_command = corba_cmd;
	}

	return cmd;
}

/**
 * gda_command_free
 */
void
gda_command_free (GdaCommand *cmd)
{
	g_object_unref (G_OBJECT (cmd));
}

/**
 * gda_command_get_cmd_type
 */
GNOME_Database_CommandType
gda_command_get_cmd_type (GdaCommand *cmd)
{
}

/**
 * gda_command_set_cmd_type
 */
void
gda_command_set_cmd_type (GdaCommand *cmd,
			  GNOME_Database_CommandType type)
{
}

/**
 * gda_command_get_text
 */
const gchar *
gda_command_get_text (GdaCommand *cmd)
{
}

/**
 * gda_command_set_text
 */
void
gda_command_set_text (GdaCommand *cmd, const gchar *txt)
{
}

/**
 * gda_command_execute
 */
GdaRecordset *
gda_command_execute (GdaCommand *cmd)
{
	GNOME_Database_Recordset corba_recset;
	CORBA_Environment ev;

	g_return_val_if_fail (GDA_IS_COMMAND (cmd), NULL);
	g_return_val_if_fail (cmd->priv->corba_command != CORBA_OBJECT_NIL, NULL);

	CORBA_exception_init (&ev);
	corba_recset = GNOME_Database_Command_execute (cmd->priv->corba_command,
						       &ev);
	if (corba_recset != CORBA_OBJECT_NIL &&
	    gda_connection_check_exception (cmd->priv->cnc, &ev)) {
		GdaRecordset *recset;

		recset = gda_recordset_new (cmd->priv->cnc, corba_recset);
		if (!GDA_IS_RECORDSET (recset)) {
			CORBA_Object_release (corba_recset, &ev);
			gda_recordset_free (recset);
			recset = NULL;
		}

		return recset;
	}

	return NULL;
}
