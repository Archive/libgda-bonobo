/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_command_h__)
#  define __gda_command_h__

#include <glib/gmacros.h>
#include <glib-object.h>

typedef struct _GdaCommand GdaCommand;

#include <gda-connection.h>
#include <gda-recordset.h>

G_BEGIN_DECLS

#define GDA_COMMAND_TYPE        (gda_command_get_type ())
#define GDA_COMMAND(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_COMMAND_TYPE, GdaCommand))
#define GDA_COMMAND_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_COMMAND_TYPE, GdaCommandClass))
#define GDA_IS_COMMAND(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_COMMAND_TYPE))
#define GDA_IS_COMMAND_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_COMMAND_TYPE))

typedef struct _GdaCommandClass   GdaCommandClass;
typedef struct _GdaCommandPrivate GdaCommandPrivate;

struct _GdaCommand {
	GObject object;
	GdaCommandPrivate *priv;
};

struct _GdaCommandClass {
	GObjectClass parent_class;
};

GType                      gda_command_get_type (void);
GdaCommand                *gda_command_new (GdaConnection *cnc);
void                       gda_command_free (GdaCommand *cmd);

GNOME_Database_CommandType gda_command_get_cmd_type (GdaCommand *cmd);
void                       gda_command_set_cmd_type (GdaCommand *cmd,
						     GNOME_Database_CommandType type);
const gchar               *gda_command_get_text (GdaCommand *cmd);
void                       gda_command_set_text (GdaCommand *cmd,
						 const gchar *txt);

GdaRecordset              *gda_command_execute (GdaCommand *cmd);

G_END_DECLS

#endif
