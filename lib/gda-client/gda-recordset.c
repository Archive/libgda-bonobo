/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-common-decls.h"
#include "gda-recordset.h"

#define PARENT_TYPE G_OBJECT_TYPE

static void gda_recordset_class_init    (GdaRecordsetClass *klass);
static void gda_recordset_instance_init (GObject *object, GTypeClass *klass);
static void gda_recordset_finalize      (GObject *object);

struct _GdaRecordsetPrivate {
	GdaConnection *cnc;
	GNOME_Database_Recordset corba_recset;

	gboolean is_eof;
};

static GObjectClass *parent_class = NULL;

/*
 * GdaRecordset class implementation
 */
static void
gda_recordset_class_init (GdaRecordsetClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_recordset_finalize;
}

static void
gda_recordset_instance_init (GObject *object, GTypeClass *klass)
{
	GdaRecordset *recset = GDA_RECORDSET (object);

	recset->priv = g_new0 (GdaRecordsetPrivate, 1);
}

static void
gda_recordset_finalize (GObject *object)
{
	GdaRecordset *recset = GDA_RECORDSET (object);

	/* free memory */
	if (recset->priv->corba_recset != CORBA_OBJECT_NIL) {
		CORBA_Environment ev;

		CORBA_exception_init (&ev);
		CORBA_Object_release (recset->priv->corba_recset, &ev);
		CORBA_exception_free (&ev);
		recset->priv->corba_recset = CORBA_OBJECT_NIL;
	}

	g_free (recset->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_recordset_get_type
 */
GType
gda_recordset_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaRecordsetClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_recordset_class_init,
			NULL,
			NULL,
			sizeof (GdaRecordset),
			0,
			(GInstanceInitFunc) gda_recordset_instance_init
		};
		type = g_type_register_static (G_TYPE_OBJECT, "GdaRecordset", &info, 0);
	}
	return type;
}

/**
 * gda_recordset_new
 */
GdaRecordset *
gda_recordset_new (GdaConnection *cnc, GNOME_Database_Recordset corba_recset)
{
	CORBA_Environment ev;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);

	CORBA_exception_init (&ev);
	if (!CORBA_Object_is_nil (corba_recset, &ev) &&
	    gda_connection_check_exception (cnc, &ev)) {
		GdaRecordset *recset;

		recset = GDA_RECORDSET (g_object_new (GDA_RECORDSET_TYPE, NULL));
		recset->priv->cnc = cnc;
		recset->priv->corba_recset = corba_recset;

		/* FIXME: call the ::describe method so that we've got a
		   desciption of all fields */
		return recset;
	}

	return NULL;
}

/**
 * gda_recordset_free
 */
void
gda_recordset_free (GdaRecordset *recset)
{
	g_object_unref (G_OBJECT (recset));
}

/**
 * gda_recordset_get_connection
 */
GdaConnection *
gda_recordset_get_connection (GdaRecordset *recset)
{
	g_return_val_if_fail (GDA_IS_RECORDSET (recset), NULL);
	return recset->priv->cnc;
}

/**
 * gda_recordset_fetch
 */
GdaRow *
gda_recordset_fetch (GdaRecordset *recset)
{
	GdaRow *row;
	CORBA_Environment ev;
	CORBA_boolean is_eof;

	g_return_val_if_fail (GDA_IS_RECORDSET (recset), NULL);
	g_return_val_if_fail (recset->priv->corba_recset != CORBA_OBJECT_NIL, NULL);

	CORBA_exception_init (&ev);
	row = GNOME_Database_Recordset_fetch (recset->priv->corba_recset, &is_eof, &ev);
	gda_connection_check_exception (recset->priv->cnc, &ev);

	CORBA_exception_free (&ev);

	recset->priv->is_eof = is_eof;

	return row;
}

/**
 * gda_recordset_is_eof
 */
gboolean
gda_recordset_is_eof (GdaRecordset *recset)
{
	g_return_val_if_fail (GDA_IS_RECORDSET (recset), TRUE);
	return recset->priv->is_eof;
}
