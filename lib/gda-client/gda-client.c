/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-client.h"
#include "gda-common-decls.h"

typedef struct {
	GdaClientInitFunc func;
	gpointer          user_data;
} idle_func_data_t;

static gboolean
client_source_cb (gpointer user_data)
{
	idle_func_data_t *func_data = (idle_func_data_t *) user_data;

	if (func_data && func_data->func)
		(* func_data->func) (func_data->user_data);
	return FALSE;
}

/**
 * gda_client_init
 * @app_id: Application name
 * @version: Application version
 * @nargs: Number of arguments
 * @args: Array of arguments to the program
 *
 * Initializes all that is needed for a GDA client to work. That is, it
 * initializes all libraries needed for a GDA client, such as OAF,
 * Bonobo, etc
 */
void
gda_client_init (const gchar *app_id, const gchar *version, gint nargs, gchar *args[])
{
	static gboolean initialized = FALSE;
	CORBA_ORB orb;

	if (initialized) {
		gda_log_error (_("Attempt to initialize an already running client"));
		return;
	}

	/* GLib initialization */
	g_set_prgname (app_id);
	g_type_init ();

	/* CORBA (OAF/Bonobo) initialization */
	orb = oaf_init (nargs, args);
	if (!bonobo_init (orb, NULL, NULL))
		g_error (_("Could not initialize Bonobo"));
	bonobo_activate ();
}

/**
 * gda_client_run
 * @func: The function to be called for client initialization
 * @data: Data to be passed to @func
 *
 * Runs a client loop. This means that your application will lose control
 * of the program execution flow, thus leaving all this work to
 * the GDA libraries. Before calling this function, you should connect
 * to the signals you're interested on for the objects you've created
 */
void
gda_client_run (GdaClientInitFunc func, gpointer data)
{
	if (func != NULL) {
		idle_func_data_t *func_data;

		func_data = g_new0 (idle_func_data_t, 1);
		func_data->func = func;
		func_data->user_data = data;

		g_idle_add ((GSourceFunc) client_source_cb, func_data);
	}

	bonobo_main ();
}

/**
 * gda_client_quit
 */
void
gda_client_quit (void)
{
	bonobo_main_quit ();
}
