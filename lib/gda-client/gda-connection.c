/* GDA Client Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <liboaf/oaf-activate.h>
#include "GNOME_Database.h"
#include "gda-common-decls.h"
#include "gda-connection.h"
#include "gda-config.h"

#define PARENT_TYPE G_OBJECT_TYPE

static void gda_connection_class_init    (GdaConnectionClass *klass);
static void gda_connection_instance_init (GObject *object, GTypeClass *klass);
static void gda_connection_finalize      (GObject *object);

struct _GdaConnectionPrivate {
	/* info about the corresponding provider */
	gchar *provider_id;
	GdaConnectionFlags flags;
	GNOME_Database_Connection corba_connection;

	/* info about the state of the connection */
	gboolean is_open;
	GdaErrorList *error_list;
	gchar *cnc_str;
	gchar *username;
	gchar *password;

	/* internal flag for disabling/enabling signal emission */
	gboolean disable_signals;

	/* list of listeners for this connection */
	GList *listener_list;
};

static GObjectClass *parent_class = NULL;

/*
 * GdaConnection class implementation
 */
static void
gda_connection_class_init (GdaConnectionClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_connection_finalize;
}

static void
gda_connection_instance_init (GObject *object, GTypeClass *klass)
{
	GdaConnection *cnc = GDA_CONNECTION (object);

	cnc->priv = g_new0 (GdaConnectionPrivate, 1);
	cnc->priv->disable_signals = FALSE;
}

static void
gda_connection_finalize (GObject *object)
{
	GdaConnection *cnc = GDA_CONNECTION (object);

	/* release CORBA stuff */
	if (cnc->priv->error_list != NULL) {
		gda_error_list_free (cnc->priv->error_list);
		cnc->priv->error_list = NULL;
	}

	if (cnc->priv->corba_connection != CORBA_OBJECT_NIL) {
		CORBA_Environment ev;

		CORBA_exception_init (&ev);
		CORBA_Object_release (cnc->priv->corba_connection, &ev);
		CORBA_exception_free (&ev);
		cnc->priv->corba_connection = CORBA_OBJECT_NIL;
	}

	/* free connection strings */
	if (cnc->priv->provider_id != NULL) {
		g_free (cnc->priv->provider_id);
		cnc->priv->provider_id = NULL;
	}
	if (cnc->priv->cnc_str != NULL)
		g_free (cnc->priv->cnc_str);
	if (cnc->priv->username != NULL)
		g_free (cnc->priv->username);
	if (cnc->priv->password != NULL)
		g_free (cnc->priv->password);

	/* free listeners */
	g_list_foreach (cnc->priv->listener_list, (GFunc) gda_listener_free, NULL);
	g_list_free (cnc->priv->listener_list);
	cnc->priv->listener_list = NULL;

	g_free (cnc->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_connection_get_type
 */
GType
gda_connection_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaConnectionClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_connection_class_init,
			NULL,
			NULL,
			sizeof (GdaConnection),
			0,
			(GInstanceInitFunc) gda_connection_instance_init
		};
		type = g_type_register_static (G_TYPE_OBJECT, "GdaConnection", &info, 0);
	}
	return type;
}

/**
 * gda_connection_new
 * @id: The id of the provider to be activated
 */
GdaConnection *
gda_connection_new (const gchar *id, GdaConnectionFlags flags)
{
	GdaConnection *cnc;
	OAF_ActivationFlags oaf_flags;
	CORBA_Environment ev;

	g_return_val_if_fail (id != NULL, NULL);

	cnc = GDA_CONNECTION (g_object_new (GDA_CONNECTION_TYPE, NULL));

	/* activate CORBA object */
	if (flags & GDA_CONNECTION_FLAGS_EXISTING_ONLY)
		oaf_flags |= OAF_FLAG_EXISTING_ONLY;

	CORBA_exception_init (&ev);
	cnc->priv->corba_connection = oaf_activate_from_id ((gchar *) id, oaf_flags, NULL, &ev);
	if (ev._major != CORBA_NO_EXCEPTION ||
	    cnc->priv->corba_connection == CORBA_OBJECT_NIL) {
		gda_connection_free (cnc);
		gda_log_error (_("Could not activate %s provider"), id);
		return NULL;
	}

	/* initialize fields */
	cnc->priv->is_open = FALSE;
	cnc->priv->provider_id = g_strdup (id);
	cnc->priv->flags = flags;
	
	return cnc;
}

/**
 * gda_connection_new_from_dsn
 * @name: Name of a configured GDA data source
 *
 * This function is a self-contained one, which does several things
 * in one shot. That is, it first looks for the given data source
 * in the GDA configuration. If it finds it, it creates a
 * #GdaConnection object by connecting it to the provider specified
 * for the given data source. Then, it tries to open a connection
 * to the underlying database system, by calling #gda_connection_open.
 *
 * Returns: a newly-open connection to the underlying database, or
 * NULL if there was any error in any of the above steps.
 */
GdaConnection *
gda_connection_new_from_dsn (const gchar *name, const gchar *username, const gchar *password)
{
	GdaConnection *cnc = NULL;
	GdaDsn *dsn;

	g_return_val_if_fail (name != NULL, NULL);

	dsn = gda_dsn_find_by_name (name);
	if (!dsn) {
		gda_log_error (_("Data source %s not found in system"), name);
		return NULL;
	}

	cnc = gda_connection_new (gda_dsn_get_provider (dsn), 0);
	if (GDA_IS_CONNECTION (cnc)) {
		/* open the connection to the database */
		if (gda_connection_open (cnc,
					 gda_dsn_get_string (dsn),
					 username ? username : gda_dsn_get_username (dsn),
					 password)
		    != GNOME_Database_ERROR_SUCCESS) {
			gda_connection_free (cnc);
			cnc = NULL;
		}
	}

	gda_dsn_free (dsn);

	return cnc;
}

/**
 * gda_connection_free
 */
void
gda_connection_free (GdaConnection *cnc)
{
	g_return_if_fail (GDA_IS_CONNECTION (cnc));
	g_object_unref (G_OBJECT (cnc));
}

/**
 * gda_connection_check_exception
 */
gboolean
gda_connection_check_exception (GdaConnection *cnc, CORBA_Environment *ev)
{
	GdaError *error;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), FALSE);
	g_return_val_if_fail (ev != NULL, FALSE);

	switch (ev->_major) {
	case CORBA_NO_EXCEPTION :
		return TRUE;
	case CORBA_SYSTEM_EXCEPTION : {
		CORBA_SystemException *sysexc;

                sysexc = CORBA_exception_value (ev);
                error = gda_error_new ();
                gda_error_set_source (error, _("[CORBA System]"));
                switch(sysexc->minor) {
                case ex_CORBA_COMM_FAILURE:
                        gda_error_set_description (error,
						   _("The server didn't respond."));
                        break;
                default:
                        gda_error_set_description (error,
						   _("An Error occured in the CORBA system."));
                        break;
                }
                gda_connection_add_error (cnc, error);
		break;
	}
	case CORBA_USER_EXCEPTION :
		if (strcmp (CORBA_exception_id (ev), ex_GNOME_Database_ProviderError) == 0) {
			GNOME_Database_ErrorSeq error_seq;
			gint idx;

			error_seq = ((GNOME_Database_ProviderError *) ev->_params)->errors;
			for (idx = 0; idx < error_seq._length; idx++) {
				error = &error_seq._buffer[idx];
				cnc->priv->disable_signals = TRUE;
				gda_connection_add_error (cnc, error);
				cnc->priv->disable_signals = FALSE;
			}
			g_signal_emit_by_name (G_OBJECT (cnc), "error", 0, cnc->priv->error_list);
		}
		break;
	default :
		error = gda_error_new ();
		gda_error_set_description (error, _("Unknown CORBA exception for connection"));
		gda_connection_add_error (cnc, error);
		break;
	}

	return FALSE;
}

/**
 * gda_connection_add_error
 */
void
gda_connection_add_error (GdaConnection *cnc, GdaError *error)
{
	g_return_if_fail (GDA_IS_CONNECTION (cnc));
	g_return_if_fail (error != NULL);

	//cnc->priv->error_list = g_list_append (cnc->priv->error_list, error);
	if (cnc->priv->disable_signals == FALSE)
		g_signal_emit_by_name (G_OBJECT (cnc), "error", 0, cnc->priv->error_list);
}

/**
 * gda_connection_add_error_list
 */
void
gda_connection_add_error_list (GdaConnection *cnc, GList *error_list)
{
	g_return_if_fail (GDA_IS_CONNECTION (cnc));
	g_return_if_fail (error_list != NULL);
}

/**
 * gda_connection_get_version
 */
gchar *
gda_connection_get_version (GdaConnection *cnc)
{
	CORBA_Environment ev;
	CORBA_char *version;
	gchar *ret = NULL;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);
	g_return_val_if_fail (cnc->priv->corba_connection != CORBA_OBJECT_NIL, NULL);

	CORBA_exception_init (&ev);

	version = GNOME_Database_Connection_getVersion (cnc->priv->corba_connection, &ev);
	if (gda_connection_check_exception (cnc, &ev)) {
		ret = g_strdup ((gchar *) version);
		CORBA_free (version);
	}

	CORBA_exception_free (&ev);

	return ret;
}

/**
 * gda_connection_get_corba_object
 */
Bonobo_Unknown
gda_connection_get_corba_object (GdaConnection *cnc)
{
	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), CORBA_OBJECT_NIL);
	return cnc->priv->corba_connection;
}

/**
 * gda_connection_open
 * @cnc: A #GdaConnection object
 * @cnc_string: The connection string
 * @username: User name
 * @password: Password to use
 *
 * Opens a connection for the given #GdaConnection object. That is,
 * a database connection is attempted on the associated database
 * server.
 *
 * Returns: error code
 */
GNOME_Database_ErrorCode
gda_connection_open (GdaConnection *cnc,
		     const gchar *cnc_str,
		     const gchar *username,
		     const gchar *password)
{
	CORBA_Environment ev;
	GNOME_Database_ErrorCode retval;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (!gda_connection_is_open (cnc), GNOME_Database_ERROR_INVALID_OPERATION);
	g_return_val_if_fail (cnc->priv->corba_connection != CORBA_OBJECT_NIL, GNOME_Database_ERROR_INVALID_OBJECT);

	CORBA_exception_init (&ev);

	retval = GNOME_Database_Connection_open (cnc->priv->corba_connection,
						 (const CORBA_char *) cnc_str,
						 (const CORBA_char *) username,
						 (const CORBA_char *) password,
						 &ev);
	if (gda_connection_check_exception (cnc, &ev)) {
		if (cnc->priv->cnc_str != NULL)
			g_free (cnc->priv->cnc_str);
		if (cnc->priv->username != NULL)
			g_free (cnc->priv->username);
		if (cnc->priv->password != NULL)
			g_free (cnc->priv->password);

		cnc->priv->cnc_str = g_strdup (cnc_str);
		cnc->priv->username = g_strdup (username);
		cnc->priv->password = g_strdup (password);
		cnc->priv->is_open = TRUE;
	}

	CORBA_exception_free (&ev);

	return retval;
}

/**
 * gda_connection_get_string
 */
const gchar *
gda_connection_get_string (GdaConnection *cnc)
{
	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);
	return (const gchar *) cnc->priv->cnc_str;
}

/**
 * gda_connection_get_username
 */
const gchar *
gda_connection_get_username (GdaConnection *cnc)
{
	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);
	return (const gchar *) cnc->priv->username;
}

/**
 * gda_connection_get_password
 */
const gchar *
gda_connection_get_password (GdaConnection *cnc)
{
	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);
	return (const gchar *) cnc->priv->password;
}

/**
 * gda_connection_is_open
 */
gboolean
gda_connection_is_open (GdaConnection *cnc)
{
	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), FALSE);
	return cnc->priv->is_open;
}

/**
 * gda_connection_close
 */
GNOME_Database_ErrorCode
gda_connection_close (GdaConnection *cnc)
{
	CORBA_Environment ev;
	GNOME_Database_ErrorCode retval;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (gda_connection_is_open (cnc), GNOME_Database_ERROR_INVALID_OPERATION);
	g_return_val_if_fail (cnc->priv->corba_connection != CORBA_OBJECT_NIL, GNOME_Database_ERROR_INVALID_OBJECT);

	CORBA_exception_init (&ev);

	retval = GNOME_Database_Connection_close (cnc->priv->corba_connection, &ev);
	if (gda_connection_check_exception (cnc, &ev))
		cnc->priv->is_open = FALSE;
	gda_connection_free (cnc); /* we unref ourselves */

	CORBA_exception_free (&ev);

	return retval;
}

/**
 * gda_connection_begin_transaction
 */
GNOME_Database_ErrorCode
gda_connection_begin_transaction (GdaConnection *cnc,
				  const gchar *name)
{
	CORBA_Environment ev;
	GNOME_Database_ErrorCode retval;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (!gda_connection_is_open (cnc), GNOME_Database_ERROR_INVALID_OPERATION);
	g_return_val_if_fail (cnc->priv->corba_connection != CORBA_OBJECT_NIL, GNOME_Database_ERROR_INVALID_OBJECT);

	CORBA_exception_init (&ev);

	retval = GNOME_Database_Connection_beginTransaction (cnc->priv->corba_connection,
							     (const CORBA_char *) name,
							     &ev);
	gda_connection_check_exception (cnc, &ev);

	CORBA_exception_free (&ev);

	return retval;
}

/**
 * gda_connection_commit_transaction
 */
GNOME_Database_ErrorCode
gda_connection_commit_transaction (GdaConnection *cnc,
				   const gchar *name)
{
	CORBA_Environment ev;
	GNOME_Database_ErrorCode retval;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (!gda_connection_is_open (cnc), GNOME_Database_ERROR_INVALID_OPERATION);
	g_return_val_if_fail (cnc->priv->corba_connection != CORBA_OBJECT_NIL, GNOME_Database_ERROR_INVALID_OBJECT);

	CORBA_exception_init (&ev);

	retval = GNOME_Database_Connection_commitTransaction (cnc->priv->corba_connection,
							      (const CORBA_char *) name,
							      &ev);
	gda_connection_check_exception (cnc, &ev);

	CORBA_exception_free (&ev);

	return retval;
}

/**
 * gda_connection_rollback_transaction
 */
GNOME_Database_ErrorCode
gda_connection_rollback_transaction (GdaConnection *cnc,
				     const gchar *name)
{
	CORBA_Environment ev;
	GNOME_Database_ErrorCode retval;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (!gda_connection_is_open (cnc), GNOME_Database_ERROR_INVALID_OPERATION);
	g_return_val_if_fail (cnc->priv->corba_connection != CORBA_OBJECT_NIL, GNOME_Database_ERROR_INVALID_OBJECT);

	CORBA_exception_init (&ev);

	retval = GNOME_Database_Connection_rollbackTransaction (cnc->priv->corba_connection,
								(const CORBA_char *) name,
								&ev);
	gda_connection_check_exception (cnc, &ev);

	CORBA_exception_free (&ev);

	return retval;
}

/**
 * gda_connection_start_logging
 */
GdaListener *
gda_connection_start_logging (GdaConnection *cnc)
{
	CORBA_Environment ev;
	GNOME_Database_ErrorCode retval;
	GdaListener *listener;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (cnc->priv->corba_connection != CORBA_OBJECT_NIL, GNOME_Database_ERROR_INVALID_OBJECT);

	/* create the listener object */
	listener = gda_listener_new ();

	/* invoke the CORBA method */
	CORBA_exception_init (&ev);

	retval = GNOME_Database_Connection_startLogging (
		cnc->priv->corba_connection,
		bonobo_object_corba_objref (BONOBO_OBJECT (listener)),
		&ev);
	gda_connection_check_exception (cnc, &ev);

	CORBA_exception_free (&ev);

	if (retval != GNOME_Database_ERROR_SUCCESS) {
		gda_listener_free (listener);
		listener = NULL;
	}
	else
		cnc->priv->listener_list = g_list_append (cnc->priv->listener_list, listener);

	return listener;
}

/**
 * gda_connection_stop_logging
 */
GNOME_Database_ErrorCode
gda_connection_stop_logging (GdaConnection *cnc, GdaListener *listener)
{
	CORBA_Environment ev;
	GNOME_Database_ErrorCode retval;

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (cnc->priv->corba_connection != CORBA_OBJECT_NIL, GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (GDA_IS_LISTENER (listener), GNOME_Database_ERROR_INVALID_OBJECT);

	CORBA_exception_init (&ev);

	retval = GNOME_Database_Connection_stopLogging (
		cnc->priv->corba_connection,
		bonobo_object_corba_objref (BONOBO_OBJECT (listener)),
		&ev);
	gda_connection_check_exception (cnc, &ev);

	CORBA_exception_free (&ev);

	/* free the listener */
	cnc->priv->listener_list = g_list_remove (cnc->priv->listener_list, listener);
	gda_listener_free (listener);

	return retval;
}
