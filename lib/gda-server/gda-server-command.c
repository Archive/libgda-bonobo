/* GDA Server Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <orbit/orbit.h>
#include "gda-common-decls.h"
#include "gda-server-command.h"

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

static void gda_server_command_class_init    (GdaServerCommandClass *klass);
static void gda_server_command_instance_init (GObject *object, GTypeClass *klass);
static void gda_server_command_finalize      (GObject *object);

struct _GdaServerCommandPrivate {
	GdaServerConnection *cnc;
	GdaServerCommandExecFunc exec_func;
	gpointer func_user_data;
	gpointer user_data;

	/* properties */
	gchar *text;
	GNOME_Database_CommandType cmd_type;
};

static BonoboObjectClass *parent_class = NULL;

/*
 * GNOME::Database::Connection CORBA interface methods
 */
static GNOME_Database_CommandType
impl_GNOME_Database_Command_getCmdType (PortableServer_Servant servant,
					CORBA_Environment *ev)
{
	GdaServerCommand *cmd = (GdaServerCommand *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), GNOME_Database_COMMAND_TYPE_INVALID);
	return gda_server_command_get_cmd_type (cmd);
}

static void
impl_GNOME_Database_Command_setCmdType (PortableServer_Servant servant,
					GNOME_Database_CommandType type,
					CORBA_Environment *ev)
{
	GdaServerCommand *cmd = (GdaServerCommand *) bonobo_x_object (servant);

	g_return_if_fail (GDA_IS_SERVER_COMMAND (cmd));
	gda_server_command_set_cmd_type (cmd, type);
}

static CORBA_char *
impl_GNOME_Database_Command_getText (PortableServer_Servant servant,
				     CORBA_Environment *ev)
{
	gchar *txt;
	GdaServerCommand *cmd = (GdaServerCommand *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), NULL);

	txt = (gchar *) gda_server_command_get_text (cmd);
	return CORBA_string_dup (txt);
}

static void
impl_GNOME_Database_Command_setText (PortableServer_Servant servant,
				     const CORBA_char *txt,
				     CORBA_Environment *ev)
{
	GdaServerCommand *cmd = (GdaServerCommand *) bonobo_x_object (servant);

	g_return_if_fail (GDA_IS_SERVER_COMMAND (cmd));
	gda_server_command_set_text (cmd, txt);
}

static GNOME_Database_Recordset
impl_GNOME_Database_Command_execute (PortableServer_Servant servant,
				     CORBA_Environment *ev)
{
	GdaServerRecordset *recset;
	GdaServerCommand *cmd = (GdaServerCommand *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), CORBA_OBJECT_NIL);

	recset = gda_server_command_execute (cmd);
	if (GDA_IS_SERVER_RECORDSET (recset))
		return bonobo_object_corba_objref (BONOBO_OBJECT (recset));

	return CORBA_OBJECT_NIL;
}

/*
 * GdaServerCommand class implementation
 */
static void
gda_server_command_class_init (GdaServerCommandClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;
	POA_GNOME_Database_Command__epv *epv;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_server_command_finalize;

	/* set the epv */
	epv = &klass->epv;
	epv->getCmdType = impl_GNOME_Database_Command_getCmdType;
	epv->setCmdType = impl_GNOME_Database_Command_setCmdType;
	epv->getText = impl_GNOME_Database_Command_getText;
	epv->setText = impl_GNOME_Database_Command_setText;
	epv->execute = impl_GNOME_Database_Command_execute;
}

static void
gda_server_command_instance_init (GObject *object, GTypeClass *klass)
{
	GdaServerCommand *cmd = GDA_SERVER_COMMAND (object);

	cmd->priv = g_new0 (GdaServerCommandPrivate, 1);
}

static void
gda_server_command_finalize (GObject *object)
{
	GdaServerCommand *cmd = GDA_SERVER_COMMAND (object);

	/* free memory */
	g_free (cmd->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_server_command_get_type
 */
GType
gda_server_command_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaServerCommandClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_server_command_class_init,
			NULL,
			NULL,
			sizeof (GdaServerCommand),
			0,
			(GInstanceInitFunc) gda_server_command_instance_init
		};
		type = bonobo_x_type_unique (
			PARENT_TYPE,
			POA_GNOME_Database_Command__init,
			NULL,
			G_STRUCT_OFFSET (GdaServerCommandClass, epv),
			&info,
			"GdaServerCommand");
	}

	return type;
}

/**
 * gda_server_command_new
 */
GdaServerCommand *
gda_server_command_new (GdaServerConnection *cnc,
			GdaServerCommandExecFunc exec_func,
			gpointer user_data)
{
	GdaServerCommand *cmd;

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), NULL);

	cmd = GDA_SERVER_COMMAND (g_object_new (GDA_SERVER_COMMAND_TYPE, NULL));
	cmd->priv->cnc = cnc;
	cmd->priv->exec_func = exec_func;
	cmd->priv->func_user_data = user_data;

	return cmd;
}

/**
 * gda_server_command_free
 */
void
gda_server_command_free (GdaServerCommand *cmd)
{
	g_object_unref (G_OBJECT (cmd));
}

/**
 * gda_server_command_get_user_data
 */
gpointer
gda_server_command_get_user_data (GdaServerCommand *cmd)
{
	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), NULL);
	return cmd->priv->user_data;
}

/**
 * gda_server_command_set_user_data
 */
void
gda_server_command_set_user_data (GdaServerCommand *cmd,
				  gpointer user_data)
{
	g_return_if_fail (GDA_IS_SERVER_COMMAND (cmd));
	cmd->priv->user_data = user_data;
}

/**
 * gda_server_command_get_connection
 */
GdaServerConnection *
gda_server_command_get_connection (GdaServerCommand *cmd)
{
	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), NULL);
	return cmd->priv->cnc;
}

/**
 * gda_server_command_get_cmd_type
 */
GNOME_Database_CommandType
gda_server_command_get_cmd_type (GdaServerCommand *cmd)
{
	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), GNOME_Database_COMMAND_TYPE_INVALID);
	return cmd->priv->cmd_type;
}

/**
 * gda_server_command_set_cmd_type
 */
void
gda_server_command_set_cmd_type (GdaServerCommand *cmd,
				 GNOME_Database_CommandType type)
{
	g_return_if_fail (GDA_IS_SERVER_COMMAND (cmd));
	cmd->priv->cmd_type = type;
}

/**
 * gda_server_command_get_text
 */
const gchar *
gda_server_command_get_text (GdaServerCommand *cmd)
{
	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), NULL);
	return (const gchar *) cmd->priv->text;
}

/**
 * gda_server_command_set_text
 */
void
gda_server_command_set_text (GdaServerCommand *cmd, const gchar *txt)
{
	g_return_if_fail (GDA_IS_SERVER_COMMAND (cmd));

	if (cmd->priv->text != NULL)
		g_free (cmd->priv->text);

	cmd->priv->text = g_strdup (txt);
}

/**
 * gda_server_command_execute
 */
GdaServerRecordset *
gda_server_command_execute (GdaServerCommand *cmd)
{
	g_return_val_if_fail (GDA_IS_SERVER_COMMAND (cmd), NULL);
	g_return_val_if_fail (cmd->priv->exec_func != NULL, NULL);

	return cmd->priv->exec_func (cmd, cmd->priv->func_user_data);
}
