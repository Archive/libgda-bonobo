/* GDA Server Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_server_command_h__)
#  define __gda_server_command_h__

#include <glib/gmacros.h>
#include <bonobo/bonobo-xobject.h>
#include <GNOME_Database.h>

/*
 * gda-server-connection.h needs GdaServerCommand to be defined
 */
typedef struct _GdaServerCommand GdaServerCommand;

#include <gda-server-connection.h>
#include <gda-server-recordset.h>

G_BEGIN_DECLS

#define GDA_SERVER_COMMAND_TYPE        (gda_server_command_get_type ())
#define GDA_SERVER_COMMAND(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_SERVER_COMMAND_TYPE, GdaServerCommand))
#define GDA_SERVER_COMMAND_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_SERVER_COMMAND_TYPE, GdaServerCommandClass))
#define GDA_IS_SERVER_COMMAND(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_SERVER_COMMAND_TYPE))
#define GDA_IS_SERVER_COMMAND_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_SERVER_COMMAND_TYPE))

typedef struct _GdaServerCommandClass   GdaServerCommandClass;
typedef struct _GdaServerCommandPrivate GdaServerCommandPrivate;

struct _GdaServerCommand {
	BonoboXObject object;
	GdaServerCommandPrivate *priv;
};

struct _GdaServerCommandClass {
	BonoboXObjectClass parent_class;

	POA_GNOME_Database_Command__epv epv;
};

typedef GdaServerRecordset * (* GdaServerCommandExecFunc) (GdaServerCommand *cmd,
							   gpointer user_data);

GType                      gda_server_command_get_type (void);
GdaServerCommand          *gda_server_command_new (GdaServerConnection *cnc,
						   GdaServerCommandExecFunc exec_func,
						   gpointer user_data);
void                       gda_server_command_free (GdaServerCommand *cmd);

gpointer                   gda_server_command_get_user_data (GdaServerCommand *cmd);
void                       gda_server_command_set_user_data (GdaServerCommand *cmd,
							     gpointer user_data);

GdaServerConnection       *gda_server_command_get_connection (GdaServerCommand *cmd);
GNOME_Database_CommandType gda_server_command_get_cmd_type (GdaServerCommand *cmd);
void                       gda_server_command_set_cmd_type (GdaServerCommand *cmd,
							    GNOME_Database_CommandType cmd_type);
const gchar               *gda_server_command_get_text (GdaServerCommand *cmd);
void                       gda_server_command_set_text (GdaServerCommand *cmd,
							const gchar *txt);

GdaServerRecordset        *gda_server_command_execute (GdaServerCommand *cmd);

G_END_DECLS

#endif
