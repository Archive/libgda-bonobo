/* GDA Server Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_server_recordset_h__)
#  define __gda_server_recordset_h__

#include <glib/gmacros.h>
#include <bonobo/bonobo-xobject.h>
#include <GNOME_Database.h>
#include <gda-row.h>

typedef struct _GdaServerRecordset GdaServerRecordset;

#include <gda-server-connection.h>

G_BEGIN_DECLS

#define GDA_SERVER_RECORDSET_TYPE        (gda_server_recordset_get_type ())
#define GDA_SERVER_RECORDSET(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_SERVER_RECORDSET_TYPE, GdaServerRecordset))
#define GDA_SERVER_RECORDSET_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_SERVER_RECORDSET_TYPE, GdaServerRecordsetClass))
#define GDA_IS_SERVER_RECORDSET(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_SERVER_RECORDSET_TYPE))
#define GDA_IS_SERVER_RECORDSET_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_SERVER_RECORDSET_TYPE))

typedef struct _GdaServerRecordsetClass   GdaServerRecordsetClass;
typedef struct _GdaServerRecordsetPrivate GdaServerRecordsetPrivate;

struct _GdaServerRecordset {
	BonoboXObject object;
	GdaServerRecordsetPrivate *priv;
};

struct _GdaServerRecordsetClass {
	BonoboXObjectClass parent_class;

	POA_GNOME_Database_Recordset__epv epv;
};

G_END_DECLS

GType                gda_server_recordset_get_type (void);
GdaServerRecordset  *gda_server_recordset_new (GdaServerConnection *cnc);

GdaServerConnection *gda_server_recordset_get_connection (GdaServerRecordset *recset);

GdaRow              *gda_server_recordset_describe (GdaServerRecordset *recset);
GdaRow              *gda_server_recordset_fetch (GdaServerRecordset *recset,
						 gboolean *is_eof);

#endif
