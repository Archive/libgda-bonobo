/* GDA Server Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "gda-server-connection.h"

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE
#define CLASS(cnc)  (G_TYPE_INSTANCE_GET_CLASS ((cnc), GDA_SERVER_CONNECTION_TYPE, GdaServerConnectionClass))

static void gda_server_connection_class_init    (GdaServerConnectionClass *klass);
static void gda_server_connection_instance_init (GObject *object, GTypeClass *klass);
static void gda_server_connection_finalize      (GObject *object);

struct _GdaServerConnectionPrivate {
	GdaErrorList *error_list;
	GList *commands;
};

static BonoboObjectClass *parent_class = NULL;

/*
 * GNOME::Database::Connection CORBA interface methods
 */
CORBA_char *
impl_GNOME_Database_Connection_getVersion (PortableServer_Servant servant,
					   CORBA_Environment *ev)
{
	return CORBA_string_dup (VERSION);
}

GNOME_Database_ErrorCode
impl_GNOME_Database_Connection_open (PortableServer_Servant servant,
				     const CORBA_char *cnc_str,
				     const CORBA_char *username,
				     const CORBA_char *password,
				     CORBA_Environment *ev)
{
	GdaServerConnection *cnc = (GdaServerConnection *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	return gda_server_connection_open (cnc, cnc_str, username, password);
}

GNOME_Database_ErrorCode
impl_GNOME_Database_Connection_close (PortableServer_Servant servant,
				      CORBA_Environment *ev)
{
	GdaServerConnection *cnc = (GdaServerConnection *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	return gda_server_connection_close (cnc);
}

GNOME_Database_Command
impl_GNOME_Database_Connection_createCommand (PortableServer_Servant servant,
					      CORBA_Environment *ev)
{
	GdaServerCommand *cmd;
	GdaServerConnection *cnc = (GdaServerConnection *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), CORBA_OBJECT_NIL);

	cmd = gda_server_connection_create_command (cnc);
	if (GDA_IS_SERVER_COMMAND (cmd)) {
		return bonobo_object_corba_objref (BONOBO_OBJECT (cmd));
	}

	return CORBA_OBJECT_NIL;
}

GNOME_Database_ErrorSeq *
impl_GNOME_Database_Connection_getErrors (PortableServer_Servant servant,
					  CORBA_Environment *ev)
{
	GdaServerConnection *cnc = (GdaServerConnection *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), NULL);
	return gda_server_connection_get_errors (cnc);
}

GNOME_Database_ErrorCode
impl_GNOME_Database_Connection_beginTransaction (PortableServer_Servant servant,
						 const CORBA_char *name,
						 CORBA_Environment *ev)
{
	GdaServerConnection *cnc = (GdaServerConnection *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	return gda_server_connection_begin_transaction (cnc, name);
}

GNOME_Database_ErrorCode
impl_GNOME_Database_Connection_commitTransaction (PortableServer_Servant servant,
						  const CORBA_char *name,
						  CORBA_Environment *ev)
{
	GdaServerConnection *cnc = (GdaServerConnection *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	return gda_server_connection_commit_transaction (cnc, name);
}

GNOME_Database_ErrorCode
impl_GNOME_Database_Connection_rollbackTransaction (PortableServer_Servant servant,
						    const CORBA_char *name,
						    CORBA_Environment *ev)
{
	GdaServerConnection *cnc = (GdaServerConnection *) bonobo_x_object (servant);

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	return gda_server_connection_rollback_transaction (cnc, name);
}

/*
 * GdaServerConnection class implementation
 */
static void
gda_server_connection_class_init (GdaServerConnectionClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;
	POA_GNOME_Database_Connection__epv *epv;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_server_connection_finalize;

	/* set the epv */
	epv = &klass->epv;

	epv->getVersion = impl_GNOME_Database_Connection_getVersion;
	epv->open = impl_GNOME_Database_Connection_open;
	epv->close = impl_GNOME_Database_Connection_close;
	epv->getErrors = impl_GNOME_Database_Connection_getErrors;
	epv->beginTransaction = impl_GNOME_Database_Connection_beginTransaction;
	epv->commitTransaction = impl_GNOME_Database_Connection_commitTransaction;
	epv->rollbackTransaction = impl_GNOME_Database_Connection_rollbackTransaction;
}

static void
gda_server_connection_instance_init (GObject *object, GTypeClass *klass)
{
	GdaServerConnection *cnc = GDA_SERVER_CONNECTION (object);

	cnc->priv = g_new0 (GdaServerConnectionPrivate, 1);
}

static void
gda_server_connection_finalize (GObject *object)
{
	GdaServerConnection *cnc = GDA_SERVER_CONNECTION (object);

	/* free memory */
	gda_error_list_free (cnc->priv->error_list);
	g_free (cnc->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_server_connection_get_type
 *
 * Registers the GdaServerConnection class if necessary, and returns
 * its unique identifier
 *
 * Returns: the unique ID of the GdaServerConnection class
 */
GType
gda_server_connection_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaServerConnectionClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_server_connection_class_init,
			NULL,
			NULL,
			sizeof (GdaServerConnection),
			0,
			(GInstanceInitFunc) gda_server_connection_instance_init
		};
		type = bonobo_x_type_unique (
			PARENT_TYPE,
			POA_GNOME_Database_Connection__init,
			NULL,
			G_STRUCT_OFFSET (GdaServerConnectionClass, epv),
			&info,
			"GdaServerConnection");
	}

	return type;
}

/**
 * gda_server_connection_construct
 */
GdaServerConnection *
gda_server_connection_construct (GdaServerConnection *cnc)
{
}

/**
 * gda_server_connection_open
 */
GNOME_Database_ErrorCode
gda_server_connection_open (GdaServerConnection *cnc,
			    const gchar *cnc_str,
			    const gchar *username,
			    const gchar *password)
{
	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (CLASS (cnc)->open != NULL, GNOME_Database_ERROR_NOT_IMPLEMENTED);

	return (* CLASS (cnc)->open) (cnc, cnc_str, username, password);
}

/**
 * gda_server_connection_close
 */
GNOME_Database_ErrorCode
gda_server_connection_close (GdaServerConnection *cnc)
{
	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (CLASS (cnc)->close != NULL, GNOME_Database_ERROR_NOT_IMPLEMENTED);

	return (* CLASS (cnc)->close) (cnc);
}

/**
 * gda_server_connection_create_command
 */
GdaServerCommand *
gda_server_connection_create_command (GdaServerConnection *cnc)
{
	GdaServerCommand *cmd;

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), NULL);
	g_return_val_if_fail (CLASS (cnc)->create_command != NULL, NULL);

	cmd = (* CLASS (cnc)->create_command) (cnc);
	if (GDA_IS_SERVER_COMMAND (cmd)) {
		cnc->priv->commands = g_list_append (cnc->priv->commands, cmd);
		/* FIXME: connect to finalize signal on 'cmd' */
	}

	return cmd;
}

/**
 * gda_server_connection_get_errors
 * cnc: A #GdaServerConnection object
 *
 * Returns the list of errors not yet reported by this connection. That is,
 * if you ask the connection to return the latest errors, it will return those
 * and clear its not-reported-errors list, so that next time you don't get
 * the same errors.
 *
 * Returns: a list of errors, which should be fred by calling #gda_error_list_free
 */
GdaErrorList *
gda_server_connection_get_errors (GdaServerConnection *cnc)
{
	GdaErrorList *ret;

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), NULL);

	/* FIXME: should we store the reported errors in some place for
	   later retrieval? or this isn't needed at all? */
	ret = cnc->priv->error_list;
	cnc->priv->error_list = NULL;

	return ret;
}

/**
 * gda_server_connection_add_error
 */
void
gda_server_connection_add_error (GdaServerConnection *cnc,
				 GdaError *error)
{
	g_return_if_fail (GDA_IS_SERVER_CONNECTION (cnc));
	g_return_if_fail (error != NULL);

	//cnc->priv->error_list = g_list_append (cnc->priv->error_list, error);
}

/**
 * gda_server_connection_add_error_string
 */
void
gda_server_connection_add_error_string (GdaServerConnection *cnc,
					GNOME_Database_ErrorCode code,
					const gchar *description)
{
	GdaError *error;

	g_return_if_fail (GDA_IS_SERVER_CONNECTION (cnc));

	error = gda_error_new ();
	gda_error_set_code (error, code);
	/* FIXME: gda_error_set_source (); */
	gda_error_set_description (error, description);

	gda_server_connection_add_error (cnc, error);
}

/**
 * gda_server_connection_begin_transaction
 */
GNOME_Database_ErrorCode
gda_server_connection_begin_transaction (GdaServerConnection *cnc,
					 const gchar *name)
{
	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (CLASS (cnc)->begin_transaction != NULL, GNOME_Database_ERROR_NOT_IMPLEMENTED);

	return (* CLASS (cnc)->begin_transaction) (cnc, name);
}

/**
 * gda_server_connection_commit_transaction
 */
GNOME_Database_ErrorCode
gda_server_connection_commit_transaction (GdaServerConnection *cnc,
					  const gchar *name)
{
	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (CLASS (cnc)->commit_transaction != NULL, GNOME_Database_ERROR_NOT_IMPLEMENTED);

	return (* CLASS (cnc)->commit_transaction) (cnc, name);
}

/**
 * gda_server_connection_rollback_transaction
 */
GNOME_Database_ErrorCode
gda_server_connection_rollback_transaction (GdaServerConnection *cnc,
					    const gchar *name)
{
	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), GNOME_Database_ERROR_INVALID_OBJECT);
	g_return_val_if_fail (CLASS (cnc)->rollback_transaction != NULL, GNOME_Database_ERROR_NOT_IMPLEMENTED);

	return (* CLASS (cnc)->rollback_transaction) (cnc, name);
}
