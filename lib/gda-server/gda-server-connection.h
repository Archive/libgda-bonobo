/* GDA Server Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_server_connection_h__)
#  define __gda_server_connection_h__

#include <glib/gmacros.h>
#include <bonobo/bonobo-xobject.h>
#include <GNOME_Database.h>
#include <gda-error.h>

/*
 * gda-server-command.h needs GdaServerConnection to be defined
 */
typedef struct _GdaServerConnection GdaServerConnection;

#include <gda-server-command.h>

G_BEGIN_DECLS

#define GDA_SERVER_CONNECTION_TYPE        (gda_server_connection_get_type ())
#define GDA_SERVER_CONNECTION(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_SERVER_CONNECTION_TYPE, GdaServerConnection))
#define GDA_SERVER_CONNECTION_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GDA_SERVER_CONNECTION_TYPE, GdaServerConnectionClass))
#define GDA_IS_SERVER_CONNECTION(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_SERVER_CONNECTION_TYPE))
#define GDA_IS_SERVER_CONNECTION_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_SERVER_CONNECTION_TYPE))

typedef struct _GdaServerConnectionClass   GdaServerConnectionClass;
typedef struct _GdaServerConnectionPrivate GdaServerConnectionPrivate;

struct _GdaServerConnection {
	BonoboXObject object;
	GdaServerConnectionPrivate *priv;
};

struct _GdaServerConnectionClass {
	BonoboXObjectClass parent_class;

	POA_GNOME_Database_Connection__epv epv;

	/* signals */

	/* virtual methods, to be implemented by providers */
	GNOME_Database_ErrorCode (* open) (GdaServerConnection *cnc,
					   const gchar *cnc_str,
					   const gchar *username,
					   const gchar *password);
	GNOME_Database_ErrorCode (* close) (GdaServerConnection *cnc);

	GdaServerCommand * (* create_command) (GdaServerConnection *cnc);

	GNOME_Database_ErrorCode (* begin_transaction) (GdaServerConnection *cnc,
							const gchar *name);
	GNOME_Database_ErrorCode (* commit_transaction) (GdaServerConnection *cnc,
							 const gchar *name);
	GNOME_Database_ErrorCode (* rollback_transaction) (GdaServerConnection *cnc,
							   const gchar *name);
};

GType                    gda_server_connection_get_type (void);
GdaServerConnection     *gda_server_connection_construct (GdaServerConnection *cnc);

GNOME_Database_ErrorCode gda_server_connection_open (GdaServerConnection *cnc,
						     const gchar *cnc_str,
						     const gchar *username,
						     const gchar *password);
GNOME_Database_ErrorCode gda_server_connection_close (GdaServerConnection *cnc);

GdaServerCommand        *gda_server_connection_create_command (GdaServerConnection *cnc);

void                     gda_server_connection_add_error (GdaServerConnection *cnc,
							  GdaError *error);
void                     gda_server_connection_add_error_string (GdaServerConnection *cnc,
								 GNOME_Database_ErrorCode code,
								 const gchar *description);
GdaErrorList            *gda_server_connection_get_errors (GdaServerConnection *cnc);

GNOME_Database_ErrorCode gda_server_connection_begin_transaction (
	GdaServerConnection *cnc, const gchar *name);
GNOME_Database_ErrorCode gda_server_connection_commit_transaction (
	GdaServerConnection *cnc, const gchar *name);
GNOME_Database_ErrorCode gda_server_connection_rollback_transaction (
	GdaServerConnection *cnc, const gchar *name);

G_END_DECLS

#endif
