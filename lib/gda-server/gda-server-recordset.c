/* GDA Server Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gda-common-decls.h"
#include "gda-server-recordset.h"

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

static void gda_server_recordset_class_init    (GdaServerRecordsetClass *klass);
static void gda_server_recordset_instance_init (GObject *object, GTypeClass *klass);
static void gda_server_recordset_finalize      (GObject *object);

struct _GdaServerRecordsetPrivate {
	GdaServerConnection *cnc;
};

static BonoboObjectClass *parent_class = NULL;

/*
 * GNOME::Database::Recordset CORBA interface methods
 */
GNOME_Database_Row *
impl_GNOME_Database_Recordset_describe (PortableServer_Servant servant,
					CORBA_Environment *ev)
{
	GdaRow *row;
	GdaServerRecordset *recset = (GdaServerRecordset *) bonobo_x_object (servant);

	row = gda_server_recordset_describe (recset);
	if (!row) {
		/* FIXME: add errors to exception */
	}

	return row;
}

GNOME_Database_Row *
impl_GNOME_Database_Recordset_fetch (PortableServer_Servant servant,
				     CORBA_boolean *is_eof,
				     CORBA_Environment *ev)
{
	gboolean tmp_is_eof;
	GdaRow *row;
	GdaServerRecordset *recset = (GdaServerRecordset *) bonobo_x_object (servant);

	row = gda_server_recordset_fetch (recset, &tmp_is_eof);
	if (!row) {
		if (!tmp_is_eof) {
			/* FIXME: add errors to exception */
		}
	}

	if (is_eof)
		*is_eof = tmp_is_eof;

	return row;
}

/*
 * GdaServerRecordset class implementation
 */
static void
gda_server_recordset_class_init (GdaServerRecordsetClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;
	POA_GNOME_Database_Recordset__epv *epv;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_server_recordset_finalize;

	/* set the epv */
	epv = &klass->epv;
	epv->describe = impl_GNOME_Database_Recordset_describe;
	epv->fetch = impl_GNOME_Database_Recordset_fetch;
}

static void
gda_server_recordset_instance_init (GObject *object, GTypeClass *klass)
{
	GdaServerRecordset *recset = GDA_SERVER_RECORDSET (object);

	recset->priv = g_new0 (GdaServerRecordsetPrivate, 1);
}

static void
gda_server_recordset_finalize (GObject *object)
{
	GdaServerRecordset *recset = GDA_SERVER_RECORDSET (object);

	/* free memory */
	g_free (recset->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/**
 * gda_server_command_get_type
 */
GType
gda_server_recordset_get_type (void)
{
	static GType type = 0;

	if (!type) {
		GTypeInfo info = {
			sizeof (GdaServerRecordsetClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_server_recordset_class_init,
			NULL,
			NULL,
			sizeof (GdaServerRecordset),
			0,
			(GInstanceInitFunc) gda_server_recordset_instance_init
		};
		type = bonobo_x_type_unique (
			PARENT_TYPE,
			POA_GNOME_Database_Recordset__init,
			NULL,
			G_STRUCT_OFFSET (GdaServerRecordsetClass, epv),
			&info,
			"GdaServerRecordset");
	}

	return type;
}

/**
 * gda_server_recordset_new
 */
GdaServerRecordset *
gda_server_recordset_new (GdaServerConnection *cnc)
{
	GdaServerRecordset *recset;

	g_return_val_if_fail (GDA_IS_SERVER_CONNECTION (cnc), NULL);

	recset = GDA_SERVER_RECORDSET (g_object_new (GDA_SERVER_RECORDSET_TYPE, NULL));
	recset->priv->cnc = cnc;

	return recset;
}

/**
 * gda_server_recordset_get_connection
 */
GdaServerConnection *
gda_server_recordset_get_connection (GdaServerRecordset *recset)
{
	g_return_val_if_fail (GDA_IS_SERVER_RECORDSET (recset), NULL);
	return recset->priv->cnc;
}

/**
 * gda_server_recordset_describe
 */
GdaRow *
gda_server_recordset_describe (GdaServerRecordset *recset)
{
}

/**
 * gda_server_recordset_fetch
 */
GdaRow *
gda_server_recordset_fetch (GdaServerRecordset *recset, gboolean *is_eof)
{
}
