/* GDA Server Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <bonobo/bonobo-generic-factory.h>
#include "gda-common-decls.h"
#include "gda-server.h"

typedef struct {
	BonoboGenericFactory *factory;
	gchar *factory_id;
	gchar *description;
	GType connection_type;
} factory_data_t;

static GList *factories = NULL;

static BonoboObject *
factory_create_func (BonoboGenericFactory *factory, void *closure)
{
	factory_data_t *factory_data = (factory_data_t *) closure;

	if (factory_data != NULL) {
		GdaServerConnection *cnc;

		cnc = GDA_SERVER_CONNECTION (g_object_new (factory_data->connection_type,
							   NULL));
		return BONOBO_OBJECT (cnc);
	}

	return NULL;
}

/**
 * gda_server_register
 * @factory_id: ID of the factory object
 * @connection_type: Type for the connection class
 *
 * Registers a new provider in the GDA space. You need to pass to this
 * function the ID of the factory for your objects, and the class type
 * of the specific class implementing the #GdaServerConnection class.
 *
 * Once you call this function, you lose control of your program, that is,
 * control won't be returned until you call (from within a signal handler,
 * for example) #gda_server_quit.
 */
void
gda_server_register (const gchar *factory_id,
		     const gchar *description,
		     gint nargs,
		     gchar *args[],
		     GType connection_type)
{
	factory_data_t *factory_data;

	g_return_if_fail (factory_id != NULL);

	factory_data = g_new0 (factory_data_t, 1);
	factory_data->factory_id = g_strdup (factory_id);
	factory_data->description = g_strdup (description);
	factory_data->connection_type = connection_type;

	/* create the Bonobo factory */
	BONOBO_FACTORY_INIT (description, VERSION, &nargs, args);
	factory_data->factory = bonobo_generic_factory_new (factory_id,
							    factory_create_func,
							    factory_data);
	bonobo_main ();
}

/**
 * gda_server_quit
 */
void
gda_server_quit (void)
{
	bonobo_main_quit ();
}
