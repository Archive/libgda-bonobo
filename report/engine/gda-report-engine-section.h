/* GDA Report Engine Library
 * Copyright (C) 2001, The Free Software Foundation
 *
 * Authors:
 *	Carlos Perell� Mar�n <carlos@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gda_report_engine_section_h__)
#  define __gda_report_engine_section_h__

#include <glib/gmacros.h>
#include <bonobo/bonobo-xobject.h>
#include <GNOME_Database_Report.h>

G_BEGIN_DECLS

#define GDA_REPORT_ENGINE_SECTION_TYPE        (gda_report_engine_section_get_type ())
#define GDA_REPORT_ENGINE_SECTION(o) \
	(G_TYPE_CHECK_INSTANCE_CAST ((o), GDA_REPORT_ENGINE_SECTION_TYPE, GdaReportEngineSection))
#define GDA_REPORT_ENGINE_SECTION_CLASS(k) \
	(G_TYPE_CHECK_CLASS_CAST((k), GDA_REPORT_ENGINE_SECTION_TYPE, GdaReportEngineSectionClass))
#define GDA_REPORT_IS_ENGINE_SECTION(o) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((o), GDA_REPORT_ENGINE_SECTION_TYPE))
#define GDA_REPORT_IS_ENGINE_SECTION_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GDA_REPORT_ENGINE_SECTION_TYPE))

typedef struct _GdaReportEngineSectionClass   GdaReportEngineSectionClass;
typedef struct _GdaReportEngineSectionPrivate GdaReportEngineSectionPrivate;

struct _GdaReportEngineSection {
	BonoboXObject object;
	GdaReportEngineSectionPrivate *priv;
};

struct _GdaReportEngineSectionClass {
	BonoboXObjectClass parent_class;

	POA_GNOME_Database_Report_Section__epv epv;
};

GType                      gda_report_engine_section_get_type (void);
GdaReportEngineSection    *gda_report_engine_section_new ();

G_END_DECLS

#endif
